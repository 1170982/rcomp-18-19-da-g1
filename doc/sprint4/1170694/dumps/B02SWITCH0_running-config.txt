!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname B02SWITCH0
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet1/1
 ip dhcp snooping trust
!
interface GigabitEthernet2/1
 ip dhcp snooping trust
!
interface GigabitEthernet3/1
 ip dhcp snooping trust
!
interface GigabitEthernet4/1
 ip dhcp snooping trust
!
interface GigabitEthernet5/1
 ip dhcp snooping trust
!
interface GigabitEthernet6/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface FastEthernet7/1
 switchport mode trunk
!
interface FastEthernet8/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface FastEthernet9/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

