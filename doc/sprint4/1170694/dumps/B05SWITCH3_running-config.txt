!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname B05SWITCH3
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport access vlan 55
!
interface GigabitEthernet1/1
 switchport access vlan 55
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 ip dhcp snooping trust
!
interface GigabitEthernet9/1
 switchport trunk allowed vlan 2-54,58,60-1001
 ip dhcp snooping trust
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

