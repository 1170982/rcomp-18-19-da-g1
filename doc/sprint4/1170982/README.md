RCOMP 2018-2019 Project - Sprint 4 - Member 1170982 folder
===========================================
During this sprint, it was necessary to configure OSPF, two servers (HTTP and DNS), NAT and ACL.

## OSPF  Routing Configuration

The chosen area for building D is the letter position in the alphabet: 4. Before anything, it was necessary to eliminate the old static routing configuration of the previous sprint. Then, each subnet IP addresses block was signed into the OSPF routing, along with an IP address of the backbone in the area 0. With this, an OSPF table will be created in the router and, with all the connections in the campus, the routing table of each router will be configured properly.



## DNS and HTTP Servers

The first step of this task was to put a new DMZ server in the building, with IP address and gateway well defined according to the pre-established rules of the previous sprints. One of the servers was configured as a DNS server, and the table of rules was defined according to the sprint backlog and as explained in the planning.

Every DHCP configuration was updated to include the connection to the DNS server, through the defined IP, and the domain name is according to the planning. The HTTP server, that had already created HTML pages, had suffered only one change: the page name went from the original to "Building D Page", in order to identify the building.

![dns](DNSTable.png)



## NAT

The IP used for the NAT configuration was one from an IP block that was not used until now and it is presented in the planning: 172.16.53.0/24. Like it was presented in the backlog, the TCP and UDP were used by the DNS service on the service ports 80 and 443, for the first, and 53 for the second.

At the end, the resulting NAT table was:

![nat](NATTable.png)

## ACL (Firewall)

For each VLAN IPs block, it was defined a list of allowed and denied operations, for UDP and for the TCP protocols. The communication between the building IPs and the HTTP servers is allowed, for UDP and TCP connections, but sniffing is denied for all the connections.

On the folder of this document exists a list of all the commands that were executed in order to create the ACL configuration that is being currently used.