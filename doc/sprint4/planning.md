RCOMP 2018-2019 Project - Sprint 4 planning
===========================================
### Sprint master: 1171138 ###

# 1. Sprint's backlog #
![backlogimage](backlog.png)

# 2. Technical decisions and coordination #

<> OSPF area id: <>

- Backbone: 0
- Building A: 1
- Building B: 2
- Building C: 3
- Building D: 4
- Building E: 5
- Building F: 6

<> DNS domain name: <>

Highest level, "root" domain:
- Building A: rcomp-18-19-da-g1

Subdomains:
- Building B: building-b.rcomp-18-19-da-g1
- Building C: building-c.rcomp-18-19-da-g1
- Building D: building-d.rcomp-18-19-da-g1
- Building E: building-e.rcomp-18-19-da-g1
- Building F: building-f.rcomp-18-19-da-g1

<> DNS name server: <>

- Building A: ns.rcomp-18-19-da-g1
- Building B: ns.building-b.rcomp-18-19-da-g1
- Building C: ns.building-c.rcomp-18-19-da-g1
- Building D: ns.building-d.rcomp-18-19-da-g1
- Building E: ns.building-e.rcomp-18-19-da-g1
- Building F: ns.building-f.rcomp-18-19-da-g1

<> IPv4 address of name server(DNS): <>

- Building A: 172.16.32.3
- Building B: 172.16.36.99
- Building C: 172.16.40.196
- Building D: 172.16.44.71
- Building E: 172.16.47.67
- Building F: 172.16.50.67

<> IPv4 address of http server: <>

- Building A: 172.16.32.2
- Building B: 172.16.36.98
- Building C: 172.16.40.194
- Building D: 172.16.44.70
- Building E: 172.16.47.66 
- Building F: 172.16.50.66

<> IP block that was not used until now(NAT): <>
![IPsNot](IPsNot.png)

- A and B had free IPs from the order sprint.

# 3. Subtasks assignment #
Building A:

The student assigned to the building A will have to:

- Substitute static routing for OSPF based dynamic routing.
- Isert OSPF into the OSPF routing protocal the default router, point to ISP router(default-information originate).
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.

Building B:

The student assigned to the building B will have to:

- Substitute static routing for OSPF based dynamic routing.
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.

Building C:

The student assigned to the building C will have to:

- Fix sprint3 phones issue and wireless connection between laptop issue.

- Substitute static routing for OSPF based dynamic routing.
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.
- Integration of every memeber's simulation.


Building D:

The student assigned to the building D will have to:

- Fix sprint3 IPv4 adress issue.

- Substitute static routing for OSPF based dynamic routing.
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers

Building E:

The student assigned to the building E will have to:

- Substitute static routing for OSPF based dynamic routing.
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers

Building F:

The student assigned to the building F will have to:

- Substitute static routing for OSPF based dynamic routing.
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.