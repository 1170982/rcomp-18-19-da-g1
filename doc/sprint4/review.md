RCOMP 2018-2019 Project - Sprint 4 review
=========================================
### Sprint master: 1171138 ###
(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog #
![backlog](backlog.png)

# 2. Subtasks assessment #
(One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.)

## 2.1. 1170561 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation), traffic access policies (static firewall) on routers and OSPF into the OSPF routing protocal the default router for building A, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.2. 1170694 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation) and traffic access policies (static firewall) on routers for building B, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.3. 1170723 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation) and traffic access policies (static firewall) on routers for building C, floors 0 and 1 and campus#
### Totally implemented with no issues. ###
## 2.4. 1170982 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation) and traffic access policies (static firewall) on routers for building D, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.5. 1170997 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation) and traffic access policies (static firewall) on routers for building E, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.6. 1171138 - OSPF based dynamic routing, second server to each DMZ network to run the HTTP service, DNS servers to establish a DNS domains tree, NAT (Network Address Translation) and traffic access policies (static firewall) on routers for building F, floors 0 and 1 #
### Totally implemented with no issues. ###

