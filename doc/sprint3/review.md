RCOMP 2018-2019 Project - Sprint 3 review
=========================================
### Sprint master: 1170561 ###
# 1. Sprint's backlog #
![backlog](backlog.png)
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

(Examples)
## 2.1. 1170561 - IPv4 addressing and routing configurations for building A #
### Totally implemented with no issues. ###

## 2.2. 1170694 - IPv4 addressing and routing configurations for building B #
### Building B: Totally implemented with no issues.

### Campus: Totally implemented with some issues.

The simulation began running slowly. To solve that, we deleted phones, pcs, laptops, servers and access points so that every building only had one of each. 
## 2.3. 1170723 - IPv4 addressing and routing configurations for building C #
### Implemented with issues. ###
The phones were not working. The wireless connection between laptop and access points were not working.
## 2.4. 1170982 - IPv4 addressing and routing configurations for building D #
### Totally implemented with no issues. ###
## 2.5. 1170997 - IPv4 addressing and routing configurations for building E #
### Totally implemented with no issues. ###
## 2.6. 1171138 - IPv4 addressing and routing configurations for building F #
### Totally implemented with no issues.