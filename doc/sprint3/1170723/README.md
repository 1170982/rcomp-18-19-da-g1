RCOMP 2018-2019 Project - Sprint 3 - Member 1170323 folder
===========================================
(This folder is to be created/edited by the team member 1170323 only)

## Sub Networking

After knowing how many nodes will be used by the building C network (325), and after the first buildings claim the networks that they needed, the building C started sub networking from the address 172.16.39.0/24.

With this, and following the pattern and schematics from auxiliary material studied during the classes regarding sub networking and the rules, including starting by giving addresses to the VLANS that requires more addresses to the less, it was possible to divide and map the network with this look:

![second](secondipdistribution.png)

With this, building C will use the following range of IP's

![first](firstipdistribution.png)

All the IPs that are unused on any sub networking were kept for future possible upgrades to the building, like, for example, a network/equipment expansion.

The router also includes the backbone network, for further possible usage.

## Changes in the Packet Tracer Simulation

### DHCP and VLANs

All the changes were on an "internal" level, with this meaning that the logical aspect was not change (excepting two phones that switched with workstations on an adjacent room) and with all the changes, mainly, occurring with the addition and configuration of a router. This router, equipped with a fiber port, will be responsible for connecting the devices to the internet and manage the IP addresses that are given to each equipment. 

The router configuration was important, due to the fact that the router itself is the equipment that will manage the IP distribution with the help of DHCP. All the VLANS received a connection and a range of IPs, as shown in the image above.

The commands given by the auxiliary material helped the configuration of the DHCP system in the router, ensuring that all the devices, when in DHCP IP configuration mode, would received the appropriate IP.

### Workstations, Laptops, APs 

The devices were all switched to obtain the network IP by DHCP protocol. With the router properly configured, it was possible to obtain instantly the correct IP addresses on this devices, simplifying the network connections. However, regarding the wireless connections between the access point and laptops, these connections were not possible, due to an unknown reason. The Cisco Packet Tracer software presents some problems in some computers, or in some projects. Upon a new project creation, the connections are easily obtain with an access point and a laptop with a proper network card, however, in this project, this was not possible. It is assumed that is it not due to human error, but to a software bug.

### VoIP Telephones Configuration

Following the auxiliary material indications, all the instructions were followed to be able to connect the VoIP phones to the network. However, this was not possible, as the VoIP phones could not perform calls. As mentioned above related to the laptops, this is due to an unknown bug, since all the files in this project follows the presented instructions, however only this one is negatively affected.
To configure a switch related to the port connected to the phones:
	int faX/1
	switchport mode access
	switchport voice vlan 64
	no switchport access vlan
In which X is the number of the port connection.

When all switches are configured, DHCP pools were needed to be created on the router, with the following commands:
	ip dhcp excluded-address 172.16.40.129 172.16.40.192
	ip dhcp pool vlan64
	option 150 ip 172.16.40.129
	network 172.16.40.128 255.255.255.192

After this, it was needed to register the phones, with the commands presented below:
telephony-service
max-ephones 8
max-dn 8
ip source-address 172.16.40.129 port 2000
auto assign 1 to 2
ephone-dn 1 
number NUMBER

Where NUMBER is the phone number, and should use the format: 00X 000 00A, where X is the building number (A = 1, B = 2 and so on), and A is the creation number of the number (1, 2, 3 and so on). On this building, the numbers used were 004 000 001 and 004 000 002. 

Building C has 8 phones available, so the "ephone" commands should be repeated 8 times.
