!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname B02ROUTER
!
!
!
!
ip dhcp excluded-address 172.16.32.129
ip dhcp excluded-address 172.16.36.1
ip dhcp excluded-address 172.16.35.129
ip dhcp excluded-address 172.16.35.1
ip dhcp excluded-address 172.16.36.97
ip dhcp excluded-address 172.16.36.65
!
ip dhcp pool vlan59
 network 172.16.36.64 255.255.255.224
 default-router 172.16.36.65
 option 150 ip 172.16.36.65
ip dhcp pool vlan55
 network 172.16.36.0 255.255.255.192
 default-router 172.16.36.1
 option 150 ip 172.16.36.1
ip dhcp pool vlan56
 network 172.16.35.128 255.255.255.128
 default-router 172.16.35.129
 option 150 ip 172.16.35.129
ip dhcp pool vlan57
 network 172.16.35.0 255.255.255.128
 default-router 172.16.35.1
 option 150 ip 172.16.35.1
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.55
 encapsulation dot1Q 55
 ip address 172.16.36.1 255.255.255.192
!
interface FastEthernet1/0.56
 encapsulation dot1Q 56
 ip address 172.16.35.129 255.255.255.128
!
interface FastEthernet1/0.57
 encapsulation dot1Q 57
 ip address 172.16.35.1 255.255.255.128
!
interface FastEthernet1/0.58
 encapsulation dot1Q 58
 ip address 172.16.36.97 255.255.255.240
!
interface FastEthernet1/0.59
 encapsulation dot1Q 59
 ip address 172.16.36.65 255.255.255.224
!
interface FastEthernet1/0.90
 encapsulation dot1Q 90
 ip address 172.16.32.131 255.255.255.192
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.16.32.130 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
telephony-service
 max-ephones 20
 max-dn 20
 ip source-address 172.16.36.65 port 2000
!
ephone-dn 1
 number 001000001
!
ephone-dn 2
 number 002000001
!
ephone-dn 3
 number 003000001
!
ephone-dn 4
 number 004000001
!
ephone-dn 5
 number 005000001
!
ephone-dn 6
 number 006000001
!
ephone 1
 device-security-mode none
 mac-address 00D0.D390.34AA
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

