RCOMP 2018-2019 Project - Sprint 3 - Member 1170694 folder
===========================================
(This folder is to be created/edited by the team member 1170694 only)

#### This is just an example for a team member with number 1170694 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170694) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 3. This may encompass any kind of standard file types.

## Sub Networking

After knowing how many nodes will be used by the building B network (272), and after the first buildings claim the networks that they needed, the building B started sub networking from the address 172.16.35.0/24.

With this, and following the pattern and schematics from auxiliary material studied during the classes regarding sub networking and the rules, including starting by giving addresses to the VLANS that requires more addresses to the less, it was possible to divide and map the network with this look:

![ips](mainIPs.png)

Subdividing the addresses of the building to each vlan, we can get th following tree:

Wi-Fi 57 --> 100 nodes : 172.16.35.0/25
First 56 --> 70 nodes : 172.16.35.128/25
Ground 55 --> 60 nodes : 172.16.36.0/26
VoIP 59 --> 30 nodes : 172.16.36.64/27
DMZ 58 --> 12 nodes : 172.16.36.96/28

All the IPs that are unused on any sub networking were kept for future possible upgrades to the building, like, for example, a network/equipment expansion.

The router also includes the backbone network, for further possible usage.

## Changes in the Packet Tracer Simulation

### DHCP and VLANs

All the changes were on an "internal" level, with this meaning that the logical aspect was not change (excepting two phones that switched with workstations on an adjacent room) and with all the changes, mainly, occurring with the addition and configuration of a router. This router, equipped with a fiber port, will be responsible for connecting the devices to the internet and manage the IP addresses that are given to each equipment. 

The router configuration was important, due to the fact that the router itself is the equipment that will manage the IP distribution with the help of DHCP. All the VLANS received a connection and a range of IPs, as shown in the image above.

The commands given by the auxiliary material helped the configuration of the DHCP system in the router, ensuring that all the devices, when in DHCP IP configuration mode, would received the appropriate IP.

### Workstations, Laptops, APs 

The devices were all switched to obtain the network IP by DHCP protocol. With the router properly configured, it was possible to obtain instantly the correct IP addresses on this devices, simplifying the network connections.

### VoIP Telephones Configuration

Following the auxiliary material indications, it was possible to connect both VoIP phones on the building to the network, allowing calls between then. Once again, this was only possible due to the correct configuration of the DHCP, which provided an address for the phone, and to the creation of "ephones" in the router that allowed the creation of numbers to the phones.

The phone numbers used the format: 00X 000 00A, where X is the building number (A = 1, B = 2 and so on), and A is the creation number of the number (1, 2, 3 and so on). On this building, the numbers used were from 002 000 001 to 002 000 004. 