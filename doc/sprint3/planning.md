RCOMP 2018-2019 Project - Sprint 3 planning
===========================================
### Sprint master: 1170561 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
![backlogimage](backlog.png)
# 2. Technical decisions and coordination #
![arvore.png](arvore.png)

After making this IP range tree, to help the team know what addresses will be used for each building, it was decided that every building will use two 24 bits range, to fulfill the sprint indications and in order that, in the future, if needed, the network of each building can be expanded a little without major problems with the addressing. With this, the range of each building is:

| Building Designation | Number of nodes indicated on the Sprint | Assignable Range            | Real capacity |
| -------------------- | --------------------------------------- | --------------------------- | ------------- |
| A                    | 249                                     | 172.16.32.1 - 172.16.34.254 | 510           |
| B                    | 272                                     | 172.16.35.1 - 172.16.38.254 | 765           |
| C                    | 325                                     | 172.16.39.1 - 172.16.41.254 | 510           |
| D                    | 347                                     | 172.16.42.1 - 172.16.44.254 | 510           |
| E                    | 357                                     | 172.16.45.1 - 172.16.47.254 | 510           |
| F                    | 336                                     | 172.16.48.1 - 172.16.50.254 | 510           |

There was a mistake divi

Each router of each building will have the first assignable address in the network attributed to itself. 

The name chosen for the routers will be {BUILDING}{FLOOR}{ROOM}SWITCH.

The phone numbers used the format: 00X 000 00A, where X is the building number (A = 1, B = 2 and so on), and A is the creation number of the number (1, 2, 3 and so on). On this building, the numbers used were 004 000 001 and 004 000 002. 


# 3. Subtasks assignment #
Building A:

The student assigned to the building A will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the MCC and one router for the ICC;
- Configuration of the ISP Router.

Building B:

The student assigned to the building B will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the ICC;
- Integration of every memeber's simulation.

Building C:

The student assigned to the building C will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the ICC.

Building D:

The student assigned to the building D will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the ICC.

Building E:

The student assigned to the building E will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the ICC.

Building F:

The student assigned to the building F will have to:

- Development of DHCP and VOIP service;
- Connection of the access points to the laptops;
- Configurate one router for the ICC.