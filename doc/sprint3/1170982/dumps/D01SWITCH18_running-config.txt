!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname D01SWITCH18
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet1/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet2/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet3/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet4/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet5/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet6/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet7/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet8/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface GigabitEthernet9/1
 ip dhcp snooping trust
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

