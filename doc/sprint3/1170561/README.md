RCOMP 2018-2019 Project - Sprint 3 - Member 1111111 folder
===========================================
## Sub Networking

After knowing how many nodes will be used by the building A network (249), and after the first buildings claim the networks that they needed, the building A started sub networking from the address 172.16.32.0/24.

With this, and following the pattern and schematics from auxiliary material studied during the classes regarding sub networking and the rules, including starting by giving addresses to the VLANS that requires more addresses to the less, building A will use the following rage of IPs:

![rangeIPS](rangeIPS.png)

All the IPs that are unused on any sub networking were kept for future possible upgrades to the building, like, for example, a network/equipment expansion.

The router also includes the backbone network, for further possible usage.

## Changes in the Packet Tracer Simulation

### DHCP and VLANs

The router connected to the IC, equipped with a fiber port, will be responsible for connecting the devices to the internet and manage the IP addresses that are given to each equipment.  The router connected to the MC will be responsible for connecting all the simulations.

The router configuration was important, due to the fact that the router itself is the equipment that will manage the IP distribution with the help of DHCP. All the VLANS received a connection and a range of IPs, as shown in the image above.

The commands given by the auxiliary material helped the configuration of the DHCP system in the router, ensuring that all the devices, when in DHCP IP configuration mode, would received the appropriate IP.

### Workstations, Laptops, APs

The devices were all switched to obtain the network IP by DHCP protocol. With the router properly configured, it was possible to obtain instantly the correct IP addresses on this devices, simplifying the network connections.

### VoIP Telephones Configuration

Following the auxiliary material indications, it was possible to connect both VoIP phones on the building to the network, allowing calls between then. Once again, this was only possible due to the correct configuration of the DHCP, which provided an address for the phone, and to the creation of "ephones" in the router that allowed the creation of numbers to the phones.

Although it is possible to create more "ephones", due to limitations and problems that lead to this situation, it were created only two and the maximum number of "ephones" currently is 2.