RCOMP 2018-2019 Project - Sprint 3 - Member 1171138 folder
===========================================
(This folder is to be created/edited by the team member 1171138 only)

## Sub Networking

After knowing how many nodes will be used by the building F network (336), and after the first buildings claim the networks that they needed, the building F started sub networking from the address 172.16.48.0/24.

With this, and following the pattern and schematics from auxiliary material studied during the classes regarding sub networking and the rules, including starting by giving addresses to the VLANS that requires more addresses to the less, it was possible to divide and map the network with this look:

- 172.16.48.0/24
	- 172.16.48.0/24
				WIFI -> 127 Nodes
	- 172.16.49.0/25
				Floor One -> 90 Nodes
	- 172.16.49.128/25
				VoIP -> 67 Nodes
	- 172.16.50.0/26
				Floor Zero -> 40 Nodes
	- 172.16.50.64/28 
				DMZ -> 12 Nodes
		
All the IPs that are unused on any sub networking were kept for future possible upgrades to the building, like, for example, a network/equipment expansion.

The router also includes the backbone network, for further possible usage.

## Changes in the Packet Tracer Simulation

### DHCP and VLANs

All the changes were on an "internal" level, with this meaning that the logical aspect was not change (excepting two phones that switched with workstations on an adjacent room) and with all the changes, mainly, occurring with the addition and configuration of a router. This router, equipped with a fiber port, will be responsible for connecting the devices to the internet and manage the IP addresses that are given to each equipment. 

The router configuration was important, due to the fact that the router itself is the equipment that will manage the IP distribution with the help of DHCP. All the VLANS received a connection and a range of IPs, as shown in the image above.

The commands given by the auxiliary material helped the configuration of the DHCP system in the router, ensuring that all the devices, when in DHCP IP configuration mode, would received the appropriate IP.

### Workstations, Laptops, APs 

The devices were all switched to obtain the network IP by DHCP protocol. With the router properly configured, it was possible to obtain instantly the correct IP addresses on this devices, simplifying the network connections.

### VoIP Telephones Configuration

Following the auxiliary material indications, it was possible to connect both VoIP phones on the building to the network, allowing calls between then. Once again, this was only possible due to the correct configuration of the DHCP, which provided an address for the phone, and to the creation of "ephones" in the router that allowed the creation of numbers to the phones.

The phone numbers used the format: 00X 000 00N, where X is the building number (A = 1, B = 2 and so on, F being 6), and N is the creation number of the number (1, 2, 3 and so on). On this building, the numbers used were 006 000 001 and 006 000 002. 

Although it is possible to create more "ephones", due to limitations and problems that lead to this situation, it were created only two and the maximum number of "ephones" currently is 20.