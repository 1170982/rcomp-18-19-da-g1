RCOMP 2018-2019 Project - Sprint 6 review
=========================================
### Sprint master: 1170997 ###
(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog #
![backlogimage](backlog.png)

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 6.1. 1170997 - Implementation of the required web-based user interface in Var application#
### Totally implemented with no issues. ###
## 6.3. 1170561 - Implementation of the required web-based user interface in Hash application#
### Totally implemented with no issues. ###
## 6.4. 1171138 - Implementation of the required web-based user interface in NumConvert application#
### Totally implemented with no issues. ###
## 6.5. 1170982 - Implementation of the required web-based user interface in Stack application#
### Totally implemented with no issues. ###
## 6.6. 1170694 - Implementation of the required web-based user interface in TCPMonitor application#
### Totally implemented with no issues. ###
## 6.7. 1170723 - Implementation of the required web-based user interface in Queue application#
### Totally implemented with no issues. ###

Since this group has 6 members, it was decided that the Task 6.2 would not be done, for the lack of a seventh member.