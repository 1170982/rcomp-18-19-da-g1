RCOMP 2018-2019 Project - Sprint 6 deployment
=========================================
### Sprint master: 1170997 ###
(This file is to be created/edited by the sprint master only)

The production host used by each team member/application was the virtual servers available through the DEI Virtual Servers Private Cloud (access via https://vs-ctl.dei.isep.ipp.pt). This cloud allowed us to create and administer a virtual server. After the creation of the server, the member that created it became its administrator. The VST chosen was VST Number 6, because although others could be chosen, would be necessary to install missing software packages for their correct administration.