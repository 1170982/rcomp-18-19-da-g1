RCOMP 2018-2019 Project - Sprint 6 planning
===========================================
### Sprint master: 1170997 ###
(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog #
![backlogimage](backlog.png)

# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this 		meeting and specified here.

Tasks:

* 1170561 - Task 6.3

* 1170694 - Task 6.6

* 1170723 - Task 6.7

* 1170982 - Task 6.5

* 1170997 - Task 6.1

* 1171138 - Task 6.4

Since this group has 6 members, it was decided that the Task 6.2 would not be done, for the lack of a seventh member.

**Task 6.1:**

The student assigned to this task will have to:

- Continue the development of Var application, implementing the required web-based user interface;

**Task 6.3:**

The student assigned to this task will have to:

- Continue the development of the Hash application, implementing the required web-based user interface;

**Task 6.4:**

The student assigned to this task will have to:

- Continue the development of the NumConvert application, implementing the required web-based user interface;

**Task 6.5:**

The student assigned to this task will have to:

- Continue the development of the Stack application, implementing the required web-based user interface;

**Task 6.6:**

The student assigned to this task will have to:

- Continue the development of the TCPmonitor application, implementing the required web-based user interface;

**Task 6.7:**

The student assigned to this task will have to:

- Continue the development of the Queue application, implementing the required web-based user interface;

As mentioned above, task 5.2 was not performed by the lack of a seventh member in the group.
