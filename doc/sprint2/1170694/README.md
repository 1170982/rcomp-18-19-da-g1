RCOMP 2018-2019 Project - Sprint 2 - Member 1170694 folder
===========================================
(This folder is to be created/edited by the team member 1170694 only)

#### This is just an example for a team member with number 1170694 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170694) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 2. This may encompass any kind of standard file types.

## VLAN Table

VLANIDs were chosen in a sequential way, to provide a stable level of organization to the team.

| VLAN ID | VLAN Number |
| ------- | ----------- |
| 55      | BGROUND     |
| 56      | BFIRST      |
| 57      | BWIFI       |
| 58      | BDMZ        |
| 59      | BVOIP       |

## Concerning the IC

The IC is represented by the switches B02SWITCH0 and B02SWITCH7. This switches, connecting to the MC, will redistribute the Internet signal and the VLAN database (VTP), and there are connections between them, in order to increase the redundancy of the connections.

Every switch has every VLAN in its local VLAN Database, belonging to every building. So the building B has assigned 5 VLANs to it ( as presented in the table above ), but every switch will have 31 (5x6 + 1). Plus one for the campus backbone.

Every switch has the same VTP domain name (vtdag1), where the B02SWITCH0 switch is in Server Mode, and the interconnections ports are in trunk mode.

All the switches are connected in Client mode, except for the B02SWITCH0 switch mentioned above.

## End-Device Used

There is, at least, one equipment per room: 
	- Workstation personal computer (PC-PT), 
	- Wireless laptop (Laptop-PT), 
	- VoIP phone (IP Phone 7960 model)
	- Server (Server-PT) 

One of the presented above was chosen to be in a room, in a balanced way, so that in the B building there would be the same number of each type of equipment.
In addition to the presented above, access points were also utilized (AccessPoint-PT). These are not located in a specific room, since in Sprint 1 they are located in the wall of the hallway, to increase internet coverage.

All the devices used are of the same type as described on the Sprint 2 description.

## Nomenclature specifications

The nomenclature used in hostnames and generic device names are the ones that were decided by the group on the planning:

- B{floor number}{room number}{equipment name}{number of the equipment}

  (for example: B02SWITCH0 refers to the Switch 0 on the building B, floor 0, room 2)


## General information

The redundancy between connecting switches (if a certain connection gets unutilizable another should exist to guarantee the connection doesn't terminate) is deployed in a triangular fashion in a new switch (B02SWITCH7) so that each switch has 2 ways to connect to the server.

All the switches are connected in Client mode, except the B02SWITCH0 switch that will be used to connect the MC to the IC. Every switch that isn't representing the IC is connected twice to another switch, to establish connection redundancy in the network. Therefore, if one cable fails, there's another that keeps the connection alive. 

## Wireless connections

These are not represented in the Sprint 2, these will be explored and implemented in a later sprint. However, the access points are represented and are not located in a specific room, decision made in the Sprint 1.