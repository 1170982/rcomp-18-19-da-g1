RCOMP 2018-2019 Project - Sprint 2 review
=========================================
### Sprint master: 1170694 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
![backlogimage](backlog.png)
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

(Examples)
## 2.1. 1170561 - Development of a layer two Packet Tracer simulation for building A, and also encompassing the campus backbone. Integration of every members� Packet Tracer simulations into a single simulation. #
### Totally implemented with no issues. ###
## 2.2. 1170694 - Development of a layer two Packet Tracer simulation for building B, and also encompassing the campus backbone. #
### Totally implemented with no issues. ###
## 2.3. 1170723 - Development of a layer two Packet Tracer simulation for building C, and also encompassing the campus backbone. #
### Totally implemented with no issues. ###
## 2.4. 1170982 - Development of a layer two Packet Tracer simulation for building D, and also encompassing the campus backbone. #
### Totally implemented with no issues. ###
## 2.5. 1170997 - Development of a layer two Packet Tracer simulation for building E, and also encompassing the campus backbone. #
### Totally implemented with no issues. ###
## 2.6. 1171138 - Development of a layer two Packet Tracer simulation for building F, and also encompassing the campus backbone. #
### Totally implemented with no issues. ###
