RCOMP 2018-2019 Project - Sprint 2 - Member 1170723 folder
===========================================
(This folder is to be created/edited by the team member 1170723 only)

#### This is just an example for a team member with number 1170723 ####
### Each member should create a folder similar to this, matching his/her number. ###

## VLAN Table ##

VLANIDs were chosen in a sequential way, to provide a stable level of organization to the team.

| VLAN ID | VLAN Number |
| ------- | ----------- |
| 60      | CGROUND     |
| 61      | CFIRST      |
| 62      | CWIFI       |
| 63      | CDMZ        |
| 64      | CVOIP       |

CGROUND: VLAN for the ground floor.
CFIRST: VLAN for the first floor.
CWIFI: VLAN for acess points.
CDMZ: VLAN for servers.
CVOIP: VLAN for phones.

Every switch has every VLAN in its local VLAN Database, belonging to every building. So the building C has assigned 5 VLANs to it ( as presented in the table above ), but every switch will have 31 (5x6 + 1). Plus one for the campus backbone.

Every switch has the same VTP domain name (vtdag1), where the switch C08SWITCH0 is in Server Mode, and the interconnections ports are in trunk mode.

All the switches are connected in Client mode, except for the switches mentioned above.

## End-Device Used

There is, at least, one equipment per room: 
	- Workstation personal computer (PC-PT), 
	- Wireless laptop (Laptop-PT), 
	- VoIP phone (IP Phone 7960 model)
	- Server (Server-PT) 

One of the presented above was chosen to be in a room, in a balanced way, so that in the C building there would be the same number of each type of equipment.
However, the Server does not follow this aspect, since it shares the room where it is with another equipment. For example, the server C08DMZ0 shares the 0.8 room with a C08PC3 PC.
In addition to the presented above, access points were also utilized (AccessPoint-PT). These are not located in a specific room, since in Sprint 1 they are located in the wall of the hallway, to increase internet coverage.

All the devices used are of the same type as described on the Sprint 2 description.

## Nomenclature specifications

The nomenclature used in hostnames and generic device names are the ones that were decided by the group on the planning:

- C{floor number}{room number}{equipment name}{number of the equipment}

  (for example: C08SWITCH1 refers to the Switch 1 on the building C, floor 0, room 8)

## General information

IC is represented by three switches, C08SWITCH0, C08SWITCH11 and C08SWITCH12, but only the C08SWITCH0 is in server mode.

Since the Switch (Switch-PT-Empty) provided by Cisco Packet Tracer only has 10 available port slots, there are situations where a switch in client mode is supposed to be connected to the switch in server mode, but due to the lack of ports, it had to be connected to another switch in client mode. The switch to make this auxilary connection was chosen at random, since this complication would only happen in this software.

## Wireless connections

These are not represented in the Sprint 2, these will be explored and implemented in a later sprint 

## Configuration process

The first step was to analyze the scheme done in Sprint 1 and adapt it to Cisco Packet Tracer. Every IC, HC and CP were transposed to the software, however, since only one equipment were required per room, the outlets were not put in Cisco Packet Tracer. Logical mode was used in this sprint 1 to represent all connections and configurations.
Since the switches used did not have ports, it was needed to add them. For copper cable connections, the PT-SWITCH-NM-1CGE ports were used, since were the fastest copper port type available. For the optic fiber ones, PT-SWITCH-NM-1FFE ports were used between MC and IC.
After all ports were set, the connections were made. Between switches, the Copper Cross-Over cables were used, and between a switch and an equipment, Copper Straight-Through cables were used.
Next phase was to set all connections between switches to trunk mode.
After this, the IC switch was put on server mode, and the rest of the switches were on client mode.
Next step, was based on the VTP domain configuration of the switch in server mode.
Then, all of the VLANs were setup respectively in the switch in server mode. Due to the correct configuration, all of these were saved in all of the switches of the building automatically. With this, every connection between a switch and an equipment was set to access mode, with the correct VLAN.
To test PC connections, we put temporary IPs on the equipments to test the different VLANs created, to ensure the quality of the connections and configurations.
For phones, it was needed to configure the voice VLAN so they could connect correctly to the VoIP VLAN.
There is also a step of cable redundancy, needed to make sure each connection is more secure, in case one fails, the other one can be used. To make sure the connections are well done, there it needs to be displayed one orange circle, since one cable is in stand by.
