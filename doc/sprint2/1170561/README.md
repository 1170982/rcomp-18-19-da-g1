RCOMP 2018-2019 Project - Sprint 2 - Member 1170651 folder
===========================================

## VLAN Table

VLANIDs were chosen in a sequential way, to provide a stable level of organization to the team.

| VLAN ID | VLAN Number |
| ------- | ----------- |
| 50      | AGROUND     |
| 51      | AFIRST      |
| 52      | AWIFI       |
| 53      | ADMZ        |
| 54      | AVOIP       |

Every switch has every VLAN in its local VLAN Database, belonging to every building. So the building C has assigned 5 VLANs to it ( as presented in the table above ), but every switch will have 31 (5x6 + 1). Plus one for the campus backbone.

Every switch has the same VTP domain name (vtdag1), where the A02SWITCH0 switch is in Server Mode, and the interconnections ports are in trunk mode. The rest of the switches are connected in Client mode.

## End-Device Uused

There is, at least, one equipment per room: 

```
- Workstation personal computer (PC-PT), 
```

```
- Wireless laptop (Laptop-PT), 
```

```
- VoIP phone (IP Phone 7960 model)
```

```
- Server (Server-PT) 
```

One of the presented above was chosen to be in a room, in a balanced way, so that in the A building there would be the same number of each type of equipment. However, since the building A only has six rooms. This means a lot of rooms have different equipments. For instance, the bigger room in the ground floor has one PC and one phone, since it is meant to be the reception of the campus. In addition to the presented above, access points were also utilized (AccessPoint-PT). 

All the devices used are of the same type as described on the Sprint 2 description.

## Nomenclature specifications

The nomenclature used in hostnames and generic device names are the ones that were decided by the group on the planning:

- A{floor number}{room number}{equipment name}{number of the equipment}

  (for example: A02SWITCH0 refers to the Switch 0 on the building A, floor 0, room 2)

## General information

Although the HC in both floors have enough ports for all the end devices, we added consolidation points either way so it would respect the scheme of the sprint 1.

For the sake of making the project easier to understand, and as said by the Sprint 2 description, the redundancy between the MC and the building IC is represented by only one optic fiber cable. For the ICs connection, it was chosen the VLAN number 90.

There is also a step of cable redundancy, needed to make sure each connection is more secure, in case one fails, the other one can be used. To make sure the connections are well done, there it needs to be displayed one orange circle, since one cable is in stand by.

## Wireless connections

These are not represented in the Sprint 2, these will be explored and implemented in a later sprint .

## Configuration process - Building A

The first step was to analyze the scheme done in Sprint 1 and adapt it to Cisco Packet Tracer. Every IC, HC and CP were transposed to the software, however, since only one equipment were required per room, the outlets were not put in Cisco Packet Tracer. Logical mode was used in this sprint 1 to represent all connections and configurations.
After all ports were set, the connections were made. Between switches, all connections between switches to trunk mode.
After this, the IC switch was put on server mode, and the rest of the switches were on client mode.
Next step, was based on the VTP domain configuration of the switch in server mode.
Then, all of the VLANs were setup respectively in the switch in server mode. Due to the correct configuration, all of these were saved in all of the switches of the building automatically. With this, every connection between a switch and an equipment was set to access mode, with the correct VLAN.
To test PC connections, we put temporary IPs on the equipments to test the different VLANs created, to ensure the quality of the connections and configurations.
For phones, it was needed to configure the voice VLAN so they could connect correctly to the VoIP VLAN.
There is also a step of cable redundancy, needed to make sure each connection is more secure, in case one fails, the other one can be used. To make sure the connections are well done, there it needs to be displayed one orange circle, since one cable is in stand by.

## Configuration process - Campus

To make all the connections between the ICs and the MC, every work from every building needed to be well configurated. That said, the first step was seeing if the ICs of each building already had all the VLANS. If so, the ICs (that were previously set on server mode) were set to client mode. With that done, the connections were made. All the ICs needed to have two FFE ports. 