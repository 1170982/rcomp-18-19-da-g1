RCOMP 2018-2019 Project - Sprint 2 - Member 1170997 folder
===========================================
(This folder is to be created/edited by the team member 1170997 only)

#### This is just an example for a team member with number 1170997 ####
### Each member should create a folder similar to this, matching his/her number. ###

## VLAN Table ##

VLANIDs were chosen in a sequential way, to provide a stable level of organization to the team.

| VLAN ID | VLAN Number |
| ------- | ----------- |
| 70      | EGROUND     |
| 71      | EFIRST      |
| 72      | EWIFI       |
| 73      | EDMZ        |
| 74      | EVOIP       |

## Concerning the IC

The IC is represented by the switches E011SWITCH0, E011SWITCH6, E011SWITCH7, E011SWITCH19 and E011SWITCH20. This switches, connecting to the MC, will redistribute the internet signal and the VLAN database (VTP). There are connections between them to increase the redundancy of the connections and to guarantee that if one cable fails, there is another to replace the connection.

The redundancy between the MC and the building IC is represented by two optic fiber cable.

Every switch has every VLAN in its local VLAN Database, belonging to every building. So the building E has assigned 5 VLANs to it (as presented in the table above), but every switch will have 31 (5x6 + 1), plus one for the campus backbone.

Every switch has the same VTP domain name (vtdag1), where the E011SWITCH0 switch is in Server Mode, and the interconnections ports are in trunk mode.

All the switches are connected in Client mode, except for the E011SWITCH0 switch mentioned above.

## End-Device Used

There is, at least, one equipment per room: 
	- Workstation personal computer (PC-PT), 
	- Wireless laptop (Laptop-PT), 
	- VoIP phone (IP Phone 7960 model)
	- Server (Server-PT) 
	- Access Point (AP-PT)

One of the presented above was chosen to be in a room, so that in the E building there would be a diferent number of each type of equipment. The Access Points are not located in a specific room, since in Sprint 1 they are located in the wall of the hallway, to increase internet coverage.

All the devices used are of the same type as described on the Sprint 2 description.

## Nomenclature Specifications

The nomenclature used in hostnames and generic device names are the ones that were decided by the group on the planning:

- E{floor number}{room number}{equipment name}{number of the equipment}

  (for example: E01SWITCH1 refers to the Switch 1 on the building E, floor 0, room 1)

## General Information

Since the Switch (Switch-PT-Empty) provided by Cisco Packet Tracer only has 10 available port slots, there are situations where a switch in client mode is supposed to be connected to the switch in server mode, but due to the lack of ports, it had to be connected to another switch in client mode. For example, the E11SWITCH8 switch is connected to the E011SWITCH6 switch, because the E011SWITCH0 switch (server mode) had no more ports available.

## Wireless Connections

These are not represented in the Sprint 2, these will be explored and implemented in a later sprint.