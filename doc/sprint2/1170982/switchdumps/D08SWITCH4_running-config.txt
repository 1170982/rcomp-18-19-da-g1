!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname D08SWITCH4
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport mode trunk
!
interface GigabitEthernet1/1
 switchport access vlan 65
!
interface GigabitEthernet2/1
 switchport access vlan 65
!
interface GigabitEthernet3/1
 switchport mode access
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
!
interface GigabitEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

