RCOMP 2018-2019 Project - Sprint 2 - Member 1170982 folder
===========================================
(This folder is to be created/edited by the team member 1170982 only)

### Each member should create a folder similar to this, matching his/her number. ###
## VLAN Table ##

| VLAN ID | VLAN Number |
| ------- | ----------- |
| 65      | DGROUND     |
| 66      | DFIRST      |
| 67      | DWIFI       |
| 68      | DDMZ        |
| 69      | DVOIP       |

## Concerning the IC

The IC is represented by the switches D01SWITCH0, D01SWITCH7, D01SWITCH18, D01SWITCH19 and D01SWITCH20. This switches, connecting to the MC, will redistribute the Internet signal and the VLAN database (VTP), and there are connections between them, in order to increase the redundancy of the connections.

## General Information

There is, at least, one equipment per room. The nomenclature used in hostnames and generic device names are the ones that were decided by the group on the planning:

- D{floor number}{room number}{equipment name}{number of the equipment}

  (for example: D01SWITCH0 refers to the Switch 0 on the building D, floor 0, room 1)

All the devices used are of the same type as described on the Sprint 2 description, namly:

- Workstation personal computer (PC-PT)
- Wireless laptop (Laptop-PT)
- VoIP phone (IP Phone 7960 model)
- Server (Server-PT) 
- Access Point (AP-PT)

Generally, there's only one equipment per room, but on every room that exists a Server equipment, there's always another equipment (usually a PC). There are cases that one switch is represented but there are two or even three on that room, connected in the HC or CP. All the switches are connected in Client mode, except the D01SWITCH0 switch that will be used to connect the MC to the IC. Every switch that isn't representing the IC is connected twice to another switch, to establish connection redundancy in the network. Therefore, if one cable fails, there's another that keeps the connection alive and stable, unless, of course, there's a failure on the switch.

## Wireless connections

These are not represented in the Sprint 2, these will be explored and implemented in a later sprint. However, the access points are represented and are not located in a specific room, decision made in the Sprint 1.