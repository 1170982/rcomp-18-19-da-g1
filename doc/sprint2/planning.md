RCOMP 2018-2019 Project - Sprint 2 planning
===========================================
### Sprint master: 1170694
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
![backlogimage](backlog.png)
# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this 		meeting and specified here.

##VLANID's to be used

###Building A 
  * Outlets in the Ground Floor --> 50
  * Outlets in the First Floor --> 51
  * Wi-Fi Access Points --> 52
  * Local Servers (DMZ) --> 53
  * VoIP --> 54
###Building B 
  * Outlets in the Ground Floor --> 55
  * Outlets in the First Floor --> 56
  * Wi-Fi Access Points --> 57
  * Local Servers (DMZ) --> 58
  * VoIP --> 59
###Building C 
  * Outlets in the Ground Floor --> 60
  * Outlets in the First Floor --> 61
  * Wi-Fi Access Points --> 62
  * Local Servers (DMZ) --> 63
  * VoIP --> 64
###Building D 
  * Outlets in the Ground Floor --> 65
  * Outlets in the First Floor --> 66
  * Wi-Fi Access Points --> 67
  * Local Servers (DMZ) --> 68
  * VoIP --> 69
###Building E 
  * Outlets in the Ground Floor --> 70
  * Outlets in the First Floor --> 71
  * Wi-Fi Access Points --> 72
  * Local Servers (DMZ) --> 73
  * VoIP --> 74
###Building F 
  * Outlets in the Ground Floor --> 75
  * Outlets in the First Floor --> 76
  * Wi-Fi Access Points --> 77
  * Local Servers (DMZ) --> 78
  * VoIP --> 79

###Connecting all buildings --> 90

##VTP
###VTP Domain name:
  * vtdag1

##Device name
  * [BuildingLetter][RoomNumber][DeviceName][DeviceNumber]

##Cable Ports Used
  * FFE --> Fiber Cable Ports FFE are for Fiber Optic Cable, which is used due to it's high velocity and high range of possible VLAN's.
  * CGE --> Copper Cable Ports CGE are for Copper Cables and have a higher data transfer speed than regular Copper Ports.

# 3. Subtasks assignment 
(For each team member (sprint master included), the description of the assigned subtask in sprint 2)

  * 1170561 --> Development of a layer two Packet Tracer simulation for building A envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches. Integration of every members� Packet Tracer simulations into a single simulation, merging all vlan's and buildings into a single Campus.
  * 1170694 --> Development of a layer two Packet Tracer simulation for building B envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches. 
  * 1170723 --> Development of a layer two Packet Tracer simulation for building C envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches. 
  * 1170982 --> Development of a layer two Packet Tracer simulation for building D envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches. 
  * 1170997 --> Development of a layer two Packet Tracer simulation for building E envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches.
  * 1171138 --> Development of a layer two Packet Tracer simulation for building F envolving placing equipments, connecting them all with certain cables and considering redundancy, setting all different vlan's and the vtp and correctly configuring all the network switches.
