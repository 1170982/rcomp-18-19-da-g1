RCOMP 2018-2019 Project - Sprint 5 - Member 1170982 folder
===========================================
(This folder is to be created/edited by the team member 1170982 only)

## Stack Program Development

During the sprint, it was committed to the repository all the code regarding the Stack application development, like a dedicated UI, controller-type class and a Stack class, that has methods from the original Stack already implemented in Java but with new methods to fulfill the tasks presented.

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI knows how to redirect the messages and receive as response the information required by the user. If the command is correct, the system knows how to get the arguments of the command, how to check them and process them as it is supposed.

The controller handles the received information and commands, already separated and ready to be used. The controller has a list of Stack objects initialized when the controller is created, but empty. The *push* command of the controller uses an identifier given by the user and a text to store in a stack on the list the information. However, if a stack with that identifier does not exist, the program creates and pushes the text. There is also a *pop* command, that pops the information out of the stack, and a *list* command, that returns a list of Stacks on the controller.

The Stack class is like an extension of the original *Stack* class of Java, but with two new methods and an identifier attribute, to fulfill the specifications.



The UI always checks, during the handling of the user input, if the request is to check the status of the program, if it is to end the program or if it is another program. On the first two conditions, the program, locally, closes itself (sending a signal to the other programs) or shows a list of open applications on the network. If it is another program, the system sends a message to the other applications with its own IP (to distinguish from another applications) to check which one is the application that was required. After that, the program waits for an answer from the other application that is received by the chat handler that prints the messages with the message *Execute|*.

If the user inputs an instruction from the UI, the program will handle it locally. On this case, giving the specifications of the backlog document, errors and input specifications are verified during the execution of the program. If everything is going according to the specifications, the program performs operations that were created in the Controller object for each case, and prints a success message to the user.



## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage. During the last period of time, the core was multiple times updated to increase stability and performance (deletion of unnecessary cycles and error correction). Before that, it was implemented the required responses to the Stack calling from other programs.

The program is capable of receiving and sending messages between programs running on the same service port. Basically, the program, to change between applications, needs to have the IP address stored on a map that is created at the beginning and updated every time that a message with *Start|* or *Stop* is received, that adds or removes an IP and a application name from the map. 