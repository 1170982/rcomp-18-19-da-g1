RCOMP 2018-2019 Project - Sprint 5 review
=========================================
### Sprint master: 1170723 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
![backlog](backlog.png)

# 2. Subtasks assessment #
(One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.)

## 2.1. 1170561 - Hash methods and features related to the messaging communication system, to allow applications to communicate with each other.#
### Totally implemented with no issues. ###
## 2.2. 1170694 - TCPMonitor methods and features related to the messaging communication system, to allow applications to communicate with each other.#
### Totally implemented with no issues. ###
## 2.3. 1170723 - Queue methods and features related to the messaging communication system, to allow applications to communicate with each other and initial project classes for team mates to implement their code.#
### Totally implemented with no issues. ###
## 2.4. 1170982 - Stack methods and features related to the messaging communication system, to allow applications to communicate with each other.#
### Totally implemented with no issues. ###
## 2.5. 1170997 - Var methods and features related to the messaging communication system, to allow applications to communicate with each other.#
### Totally implemented with no issues. ###
## 2.6. 1171138 - NumConvert methods and features related to the messaging communication system, to allow applications to communicate with each other.#
### Totally implemented with no issues. ###


