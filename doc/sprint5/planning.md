RCOMP 2018-2019 Project - Sprint 5 planning
===========================================
### Sprint master: 1170723 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
![backlogimage](backlog.png)

# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this 		meeting and specified here.

Tasks:
  * 1170561 - Task 5.3
  * 1170694 - Task 5.6
  * 1170723 - Task 5.7
  * 1170982 - Task 5.5
  * 1170997 - Task 5.1
  * 1171138 - Task 5.4

Since this group has 6 members, it was decided that the Task 3.2 would not be done, for the lack of a seventh member.

Task 5.1:

The student assigned to this task will have to:

- Develop the Var applications, with all commands instructed;

Task 5.3:

The student assigned to this task will have to:

- Develop the Hash applications, with all commands instructed;

Task 5.4:

The student assigned to this task will have to:

- Develop the NumConvert applications, with all commands instructed;

Task 5.5:

The student assigned to this task will have to:

- Develop the Stack applications, with all commands instructed;

Task 5.6:

The student assigned to this task will have to:

- Develop the TCPmonitor applications, with all commands instructed;

Task 5.7:

The student assigned to this task will have to:

- Develop the Queue applications, with all commands instructed;

Besides individual applications, each one of the members will have to implement the special commands "Status" and "exit" and implement the respective Core part,
related to it's application, to allow all applications to have communications with each other.

As mentioned above, task 5.2 was not performed by the lack of a seventh member in the group.


