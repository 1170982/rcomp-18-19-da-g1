RCOMP 2018-2019 Project - Sprint 5 - Member 1171138 folder
===========================================
(This folder is to be created/edited by the team member 1171138 only)

## Number Convert Program Development

During the sprint, it was committed to the repository all the code regarding the Number Convert application development, like a dedicated UI, controller-type class related to Number Convert, that has methods from the original convertions that already are  implemented in Java but with new methods to fulfill the tasks presented.

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI knows how to redirect the messages and receive as response the information required by the user. If the command is correct, the system knows how to get the arguments of the command, how to check them(using the list of identifiers) and process them as it is supposed.
 
One of the options is "convert" that is redirect to the method convert in the controller, that analysis the ir, or, number arguments and by using ifs enters the correct convertion. For example, if ir=BIN (binary) and or=HEX (hexadecimal), the application starts by converting the number (thats is a string) into decimal format (int) and then uses a already existing method in java that converts decimal to hexadecimal format (string) and returns it. If the number is not in the right format, for example, using numbers that aren't 0 and 1, the application gives an alert to the user.

The other one is "list" that lists the identifiers use as IR and OR arguments. The identifiers being BIN for binary, OCT for octal, HEX for hexadecimal and DEC for decimal.

## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage.

Before that, it was implemented the required responses to the Number Convert calling from other programs. Meaning that if a user trys to acess the application in another application, the same one will redirect to the right one. To do that that all the mini-applications use the same service port (30101).