RCOMP 2018-2019 Project - Sprint 5 - Member 1170723 folder
===========================================
(This folder is to be created/edited by the team member 1170723 only)

## Queue Program Development

During the sprint, it was committed to the repository all the code regarding the Queue application development, like a dedicated UI, controller-type class and a Queue class, that has methods from the original Queue already implemented in Java but with new methods to fulfill the tasks presented.

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI knows how to redirect the messages and receive as response the information required by the user. If the command is correct, the system knows how to get the arguments of the command, how to check them and process them as it is supposed.

The controller handles the received information and commands, already separated and ready to be used. The controller has a list of Queue objects initialized when the controller is created, but empty. The *Enqueue* command will allow the user to add a string to the queue with an identifier. If the identifier doesn't exist, a new queue is created. As a response, it shows the user the number representing the queue size after the enqueue operation. The *Dequeue* command will allow the user to remove the oldest element from a queue with an identifier. If the identifier doesn't exist, a suitable error is showed the user. If however no elements are left, the queue is left empty, but it is not removed. The *Remove* command will allow the user to remove a certain queue with an identifier from the queue list, erasing all elements from it aswell. If the identifier doesn't exist, a suitable error message is shown, otherwise a suitable success is shown. The *List* command will allow the user to see all of the queues present in the system, with the corresponding number of elements present in each.

The Queue class is like an extension of the original *Queue* class of Java, but with four new methods and an identifier attribute, and queueData attribute, to fulfill the specifications.

## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage.

Before that, it was implemented the required responses to the Queue calling from other programs.