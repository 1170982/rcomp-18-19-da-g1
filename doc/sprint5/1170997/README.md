RCOMP 2018-2019 Project - Sprint 5 - Member 1170997 folder
===========================================
(This folder is to be created/edited by the team member 1170997 only)

## VAR Program Development

During the sprint, it was committed to the repository all the code regarding the Var application development, like a dedicated UI, controller-type class and a Var class, that has methods from the original Var already implemented in Java but with new methods to fulfill the tasks presented.

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI knows how to redirect the messages and receive as response the information required by the user. If the command is correct, the system knows how to get the arguments of the command, how to check them and process them as it is supposed.

The controller handles the received information and commands, already separated and ready to be used.
The controller has a list of Var objects initialized when the controller is created, but empty. 
The *store* command of the controller uses an identifier given by the user and a text to store the information in a var on the list. On the one hand, if the text exceeds the size of 400 bytes, then the var is not saved or if the text is empty, the var is removed from the list. On the other hand, if a var with that identifier does not exist, the program creates and stores the text or if already exist one, then the program stores the text in the var with that identifier.
The *fetch* command uses an identifier given by the user. If exist a var with that identifier, then fetches its content and the stored text content is returned, if the var does not exist is returned an error message.
The *list* command for each var adds the identifier to a list and calculates the sum of the message bytes, if it exceeds the maximum message bytes, then the last identifier is removed from the list and the list with the identifiers is returned.
The *erase* command uses an identifier given by the user. If a var with that identifier does not exist, the program return an error message, else if a var already exists, its content is fetched, the var is removed from the list and the fetched content is returned.

## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage.

Before that, it was implemented the required responses to the Stack calling from other programs.