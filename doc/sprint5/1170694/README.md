RCOMP 2018-2019 Project - Sprint 5 - Member 1170694 folder
===========================================
## TCP Monitor Program

During the sprint, it was committed to the repository all the code regarding the TCPmonitor application development, like a dedicated UI, controller-type class and a Service class. This same service has as attributes an identifier, an IP address and a TCP port number (which will be used to make the connection to the test server).

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI redirects the message and receives as response the information required by the user. If the command is correct, the system processes the command splitting it into arguments, check them and executing them as it is supposed.

The controller handles the received information and commands, already separated and ready to be used.

When started, the application will auto-run in a new thread a TCP server using the default port of the core application (30101) and with the IP address of the PC in which is running.

When the used command is "addService", the program retrieves the arguments that follow the "addService" command into seperate strings and creates a new Service. The new Service will also be marked as UNCHECKED until the "servicesStatus" method is executed. If the given identifier for the new Service also belongs to an existant Service in the program, it will be replaced by the new one. If the command is executed successfully, the application will return a success message. If an error occurs, an error message will also be displayed.

When the used command is "removeService", the program will get the argument passed in the command and use it as an identifier for an existant Service (to be removed). If the Service is removed successfully, a success message will be displayed. If the program doesn't recognise the identifier or another error occurs, an error message is presented.

When the used command is "servicesStatus" the program will test all the previously added Services for a successful connection to the TCP test server created in the start of the application. If the Service connects successfully to the server, an AVAILABLE status will be outputted and saved in that Service. If the connection fails or a timeout occurs (timeout 5 seconds), the Service will be marked as UNAVAILABLE and the same status will also be presented.

## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage.

Before that, it was implemented the required responses to the TCPmonitor calling from other programs.