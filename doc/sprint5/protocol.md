RCOMP 2018-2019 Project - Sprint 5 protocol
===========================================
### Sprint master: 1170723 ###
(This file is to be created/edited by the sprint master only)

<> Input format: <>

User-messages:

Application name|Argument1|Argument2|Argument3|...

In which:
	- "Application name" corresponds to the name that identifies that application in the system;
	- "Argument1" is the first, or only argument that the unser inputs in the application.
	- From application to application, it may be required more than 1 argument. Each of these are separated with a "|", as shown above.

To call a method present in an Application, the following format was decided:

Method(argument1,argument2)

In which:
	- "Method" corresponds to the command present in the application
	- "argument1" corresponds to an arguments that the method in questions needs.
	- From method to method, it may be required more than 1 argument. Each of these are separated with a ",", as shown above.

Every member follows this last format, except for the member with task 5.3, which chose to follow the following format:

Method|argument1|argument2

The only difference are in the data seperation, reflected on the lack of paranthesis and commas.

# UDP port number #
30101, as instructed for Team 1

# Broadcast IP Address #
255.255.255.255, by default

# List of IP addresses #

The list "listApplicationsIp" will have the IP address of every active application in the system at a certain time, located in the UdpChatReceive Class.

# Keywords in the Communication Messaging System related to System Messages #

"ChangeApp" - Messages with this keyword have the purpose to be sent when a user enters a command that does not belong to that application. For example, if the user, within the "Stack" application, wants to add an element to a queue with the "Enqueue" command from the "Queue" application, to allow the system to find the correct application to run the command

"Execute" - Messages with this keyword mean that they have information that should be printed for the user. As the example stated above exemplifies, the message with this keyword would contain the information from the output of the operation "Enqueue" to be shown to the user.

"Start" - Messages with this keyword are sent by applications that were just started in the system. By doing this, it allows all the other applications to update their list of active applications, by knowing that another application has started.

"End"- Messages with this keyword are sent by applications that are about to end. By doing this, it allows all the other applications to update thei list of active applications, by knowing that this application has just ended, and should be removed from said list.

# General Class Organization #

This project is seperated  between user interfaces (UI), controllers, and domain.
The UI is made up by the mainUI class, as well individual application UI, each individually configured to show the user specific commands.
The controllers seperate the UI from the Domain classes for better organization and business rules, aswell as contain the methods used by the user.
The domain contains the application calculation classes, which will they will be used with the controllers to produce the outputs to be shown to the user, aswell as
containing the Core and UdpChatReceive classes, that come up with the communication messaging system, that allow the applications to communicate with each other.

# Project flow between classes #

< Core >

Core class will have the responsibility of setting up the system that sends the various messages to the applications, by creating a socket using the default IP 255.255.255.255 and the port 30101. With this, will be possible to stablish communication with only the applications from this group, since each group has a different port. Utililzing a udpReceiver thread, it will allow the program to be a multitasking one. In another words, the normal application flow will not be negatively affected by messages sent or received. After this, the main user interface is called for the user to choose which application to run.

< MainUI >

In the mainUI class, the user will be allowed to choose the application he wants to run. On top of that, the system will send a message representing the start of the application to any already running applications, for the reasons presented above in the "Keyword Start" section, aswell calling the respective application UI.

< Applications >

Depending on what application the user chooses, different outputs are shown, however, every application is called from the mainUI class, and utilize different controllers and specific UI.

< UdpChatReceive >

Every applications connects to this class through the "message" or "messageTo" methods present in the Core class. They will allow for the actual information to be sent
for the class to receive it. After this process, the message is interpreted in the UdpChatReceive class, through the key words analyzed above.
The applicationRedirection will allow for each application and each method to have a specific message to send back to the system as output for the user to see. Each one is seperated by an if statement, containing specific parts of the UI, aswell as specific messages with information for the user, in case this put a command from a different application he was already in.
