RCOMP 2018-2019 Project - Sprint 5 - Member 1170561 folder
===========================================
## Hash Program Development

During the sprint, it was committed to the repository all the code regarding the Hash application development, like a dedicated UI, controller-type class and a Hash class, that has methods from the original Hash already implemented in Java but with new methods to fulfill the tasks presented.

The command UI shows a list of options to use in a local application and shows how to execute the options. If an option that does not exist or is from another network program is called, the UI knows how to redirect the messages and receive as response the information required by the user. If the command is correct, the system knows how to get the arguments of the command, how to check them and process them as it is supposed.

The controller handles the received information and commands, already separated and ready to be used. 

When the command is "Digest", the information can go to one of three controllers related to the calculation of the Hash code. Those three controllers redirect the information they receive all to the same method located in the Hash class. However, depending on the controller, the information sent can be MD5, SHA-0 or SHA-256. The result is the hash code of an expression sent in hexadecimal.

When the comman is "List", the method in the controller returns a list with the ids that can be used: MD5, SHA-0 or SHA-256.

## Core Development

The changes to the core program were made during the sprint in order to improve and handle better the errors, besides correcting some bugs that existed during a first stage.

Before that, it was implemented the required responses to the Hash calling from other programs.