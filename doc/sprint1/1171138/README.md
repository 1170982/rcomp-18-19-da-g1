RCOMP 2018-2019 Project - Sprint 1 - Member 1171138 folder
===========================================
(This folder is to be created/edited by the team member 1171138 only)

### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1171138) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.

## Area Calculation

![Measurement Table](Measurement Table.png)



## Outlet and Positions

The brown color represents cables coming out of a HC, gray cables are IC - HC connections.

![F0 Outlet and Cable Position](Building-F-F0.PNG)





![F1 Outlet and Cable Position](Building-F-F1.PNG)

The outlets are installed on the walls and ceiling and were distributed in order to increase the cable coverage inside the rooms. For each 2 rooms (or 3, on some exceptions), there is an HC to make the network distribution, and all the HC are connected directly to the IC, on floor 0. This method increases each room independency to network access and it is easier to make maintenance. Each cable follows a certain pathway, that most of the time is common to another cables in order to keep the design simple and clean.

Each HC has a maximum number of 32 cables connected to the equipment, which means that, in some cases, the HC will host 2 switches.



## Inventory

- 6489,81m CAT6 copper cables
- 1,40m optic fiber cable
- 388 outlets
- 18 racks, size 1U 
- 23 24-port switches
- 776 RJ45
- 6 wireless access points