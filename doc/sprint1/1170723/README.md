RCOMP 2018-2019 Project - Sprint 1 - Member 1170723 folder
===========================================
(This folder is to be created/edited by the team member 1170723 only)

#### This is just an example for a team member with number 1170723  ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170723) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

## Area Calculation (Length and Widths)

![Measurement Book with Size Specifications](Measurement Book Building C.xlsx)

## Outlet Distances and Calculations

This file contains all the cables lengths and number of switches needed, divided by rooms and horizontal cross-connects, 
and hardware inventories.

![Outlet Distances and Calculations](Outlet Distance and Calculations Book - Building C.xlsx)

## Outlet Distances and Calculations

## Ground Floor (C0)

# Main Floor Cable Scheme

Each HC has its own colored cables for analysis support.
The cable representation is exemplified on a 2D plane, however, all the cables are located on the walls, at switch-height level.

![Ground Floor Main Scheme](C0 Cables and Outlets.png)

# Outlet Legend

Each outlet has its number for identification in the Outlet Distances and Calculations file.

![Ground Floor Outlet Legend](C0 Outlet Legend.png)

# Auxilary Measurements

Along with the main floor cable scheme, this image contains additional measurements to better understand the floor's dimensions.

![Ground Floor Auxilary Measurements](C0 Auxilary Measurements.png)

## First Floor (C1)

# Main Floor Cable Scheme

Each HC has its own colored cables for analysis support.
With access to a removable dropped ceiling, all cables in a diagonal disposition are considered to be installed in it.
Ceiling height was considered in the cables' lengths calculations.

![First Floor Main Scheme](C1 Cables and Outlets.png)

# Outlet Legend

Each outlet has its number for identification in the Outlet Distances and Calculations file.

![First Floor Outlet Legend](C1 Outlet Legend.png)

# Justifications

In every wall of every room, all outlets were placed equidistantly, to allow the user to have a bigger area of access.
Each room's area was calculated to find the exact number of mandatory outlets needed.
Cross-connects were placed in a way to use the least ammount of cables possible, each one hosting, generally, connections to 2 or 3 different rooms.
These numbers were chosen having in mind the relation between the total cost of the horizontal-cross connects (telecommunication enclosure and switches ) and the 
total cost of cables. 
In the first floor (C1), with the existence of a removable dropped ceiling, this was used when necessary, since the cable would need to go up and down the wall ( 5 meters in total ).
24 ports switches were chosen compared to the 48 ports variety, since it allows a certain level of security. In other words, if the 48 port switch failed, it would have a bigger negative impact compared to a
24 port one.
The IC was placed near the entrance so the optic fiber cable was used the least possible.

# Inventory

- 4104,259m CAT6 copper cables
- 2,18m optic fiber cable
- 346 outlets
- 10 racks, size 1U 
- 21 24-port switches
- 4 Wireless access points
- 692 RJ45

