RCOMP 2018-2019 Project - Sprint 1 - Member 1170997 folder
===========================================
(This folder is to be created/edited by the team member 1170997 only)

#### This is just an example for a team member with number 1170997  ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170997) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

## Outlet and Positions

The red lines represents cables coming out of a HC to an outlet and blue cables are IC to HC connections.

![E0 - Outlet Position](GroundFloor.PNG)





![E1 - Outlet Position](FirstFloor.PNG)

The outlets are installed in order to increase the cable coverage and maintenance inside the rooms. For each 2 rooms (or 3, on some exceptions), there is an HC to make the network distribution, and all the HC are connected directly to the IC, on ground floor. This method has been adopted to reduce damages if any HC fails. Each cable follows a certain pathway, that most of the time is common to another cables in order to keep the design simple and clean.

Each HC has a maximum number of 32 cables connected to the equipment, which means that, in some cases, the HC will host 2 switches.



## Inventory

- 6783,64m CAT6 copper cables
- 51,67m optic fiber cable
- 388 outlets
- 16 racks, size 1U 
- 22 24-port switches
- 776 RJ45
- 5 wireless access points