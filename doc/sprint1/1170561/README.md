RCOMP 2018-2019 Project - Sprint 1 - Member 1170561 folder
===========================================
### Building A ###

The building A is the one with the main cross-connect, which means it will give network to the other buildings. It has one campus distributor in the ground floor, and there's only one like it in the whole campus. Back bone redundancy was used, which means there are two cables doing the same path between building A (where the MC is) and the other buildings.

![Campus](Campus.png)

#### Ground floor

The ground floor has the intermediate cross-connect, which is connected to the MC by a optical fibre cable. Since this floor has underfloor cable raceway, I didn't feel the need to drill the floor specially in the A0.2 room. However, it was needed in the A0.1 and A0.3 rooms.

The copper cable chosen was the CAT6. It was only needed one HC with two switches of  24 ports each, since the total floor has 49 outlets. One of the connections in one switch is for a consolidation point. This means one switch will be connected to the CP, to 3 outlets (A0.2 room), to the 5 outlets in the A0.1 room and to the other switch. The other one will be connected to the 18 outlets in the A0.3 room. There were chosen two switches of 24 ports instead of one of 48 because because if the second one fails, it will only afect 18 outlets and not the 49. The outlet for the wi fi is placed in the inferior left corner of the A0.2 room since that place can reach every corner in the floor (all places are less than 30 meters away).

![Floor 0](edAfloor0edit.png)

#### Floor 1

The copper cable chosen was the CAT6. The first floor also only needs one HC with two switches of 24 ports each, since the floor has 50 outlets. One of the connections in one switch is for a consolidation point. This means one switch will be connected to the CP, to 8 outlets (A1.5 room) and to the other switch. The other one will be connected to the 18 outlets in the A1.2 room. The CP will be connected to the 24 outlets in the A1.1 room. There were chosen two switches of 24 ports instead of one of 48 because because if the second one fails, it will only afect 18 outlets and not the 50.

![Floor 1](edAfloor1edit.png)



