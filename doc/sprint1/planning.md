RCOMP 2018-2019 Project - Sprint 1 planning
===========================================
### Sprint master: 1170982 ###
# 1. Sprint's backlog #
![backlogimage](backlog.jpg)
# 2. Technical decisions and coordination #
  * When is possible, it is obligatory to use the drop ceiling
  * All the measures must be taken and registered on appropriate sheets
  * HC are used to host switches, normally
# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 1)

#### Example: ####
  * 1170561 - Task 1.1
  * 1170694 - Task 1.2
  * 1170723 - Task 1.3
  * 1170982 - Task 1.4
  * 1170997 - Task 1.5
  * 1171138 - Task 1.6
