RCOMP 2018-2019 Project - Sprint 1 - Member 1170694 folder
===========================================
(This folder is to be created/edited by the team member 1170694 only)

#### This is just an example for a team member with number 1170694 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170694) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

## Outlet and Positions: 

Legend for each schematic is exemplified in it.

![B0 Outlet Position](groundFloor.png)

On the ground floor, will have the IC for the building located in room B0.2. This room will have to be access-restricted and it's use limited to network management. The outlet layout for the entire floor will be a set of "hotspots" (maximum 8 per room) each containing a number of outlets in accordance to the specified rule. The Wi-Fi routers only exist in this floor being that the internet can reach the entire ground and first floor. 

![B1 Outlet Position](1stFloor.png)

On the top floor, 3 HC's were necessary to manage the entire floor. Deployed on strategic locations, they cover a maximum of 4 rooms and 46 outlets per HC. Being that this floor contains a removable dropped ceiling covering the entire floor, certain cables will be passing through to reduce the cable quantity. However, a decision was made to only perfurate the ceiling once per room to avoid several holes on it.

#Equipment:

##388 RJ45 ports
##6 racks size 1U1
##9 switches 24-port
##3.446 Km copper cable CAT6
##22.95 m optic fiber cable

## 188 outlets