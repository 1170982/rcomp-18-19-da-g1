RCOMP 2018-2019 Project - Sprint 1 review
=========================================
### Sprint master: 1170982 ###
# 1. Sprint's backlog #
![backlog](backlog.jpg)
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1170561 - Structured cable design for building A, floors 0 and 1; encompassing the campus backbone #
### Totally implemented with no issues. ###
## 2.2. 1170694 - Structured cable design for building B, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.3. 1170723 -  Structured cable design for building C, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.4. 1170982 - Structured cable design for building D, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.5. 1170997 - Structured cable design for building E, floors 0 and 1 #
### Totally implemented with no issues. ###
## 2.6. 1171138 - Structured cable design for building F, floors 0 and 1 #
### Totally implemented with no issues. ###