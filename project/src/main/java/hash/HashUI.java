package hash;

import shared.Core;
import shared.RepeatedCalls;
import shared.UdpChatReceive;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * UI of the package Hash.
 */
public class HashUI {

    /**
     * Scanner to read messages from the outside.
     */
    private static Scanner in = new Scanner(System.in);

    /**
     * Attribute controller of the class.
     */
    private HashController hashController;

    /**
     * Constructor of the class.
     */
    public HashUI() {
        hashController = new HashController();
        UdpChatReceive.setHashController(hashController);
        System.out.println(" ### Hash Application ### ");
    }

    /**
     * Presentation: this class has the messages shown to the user.
     *
     * @param udpPacket
     * @throws Exception
     */
    public void presentation(DatagramPacket udpPacket) throws Exception {
        boolean continueUI = true;
        RepeatedCalls rc = new RepeatedCalls("Hash|Start",udpPacket);
        rc.canGo(true);
        rc.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- Digest(identifier,text)");
            System.out.println("- List");
            System.out.println("Insert your code with the correct format: (0 or Exit to End Application)");
            code = in.nextLine();

            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: " + entry.getKey() + "\nIP Address: " + entry.getValue());
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "Hash|End");
                continueUI = false;
            } else if (subCodeParts[0].equals("Queue") || subCodeParts[0].equals("NumConvert") || subCodeParts[0].equals("Stack") || subCodeParts[0].equals("TcpMonitor") || subCodeParts[0].equals("Var")) {
                System.out.println("In the wrong application, redirecting to the correct one...");

                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            } else if (!subCodeParts[0].equals("Hash")) {
                System.out.println("Application does not exist !");
            }
            /*if (!subCodeParts[0].equals("Hash")) {
                //reencaminhar para sistema
                System.out.println("In the wrong application, redirecting to the correct one...");
            }*/
            else if (subCodeParts.length == 4 || subCodeParts.length == 2) {
                if (subCodeParts[1].equals("Digest") && subCodeParts[2].equals("MD5")) {
                    String hashC = hashController.hashMD5(subCodeParts[3]);
                    System.out.println("The hash code in MD5 is: " + hashC + ".");
                } else if (subCodeParts[1].equals("Digest") && subCodeParts[2].equals("SHA-0")) {
                    String hashC = hashController.hashSHA0(subCodeParts[3]);
                    System.out.println("The hash code in SHA-0 is: " + hashC + ".");
                } else if (subCodeParts[1].equals("Digest") && subCodeParts[2].equals("SHA-256")) {
                    String hashC = hashController.hashSHA256(subCodeParts[3]);
                    System.out.println("The hash code in SHA-256 is: " + hashC + ".");
                } else if (subCodeParts[1].equals("List")) {
                    List<String> ids = hashController.hashList();
                    System.out.println("The identifiers available are:");
                    for (String s : ids) {
                        System.out.println(s);
                    }
                } else {
                    System.out.println("Invalid input. Please try again.");
                }
            } else
                System.out.println("Invalid input. Please try again.");
        }
        rc.canGo(false);
        rc.join();
    }
}

