package hash;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller of the hash class.
 */
public class HashController {

    /**
     * Attribute of the hash class.
     */
    private Hash hash;

    /**
     * Constructor of the controller class.
     */
    public HashController(){
        hash = new Hash();
    }

    /**
     * Controller when the id used is MD5.
     * @param subCodePart text to be transformed
     * @return hashCode in MD5 form
     */
    public String hashMD5(String subCodePart) {
        return hash.hash("MD5",subCodePart);
    }

    /**
     * Controller when the id used is SHA-0.
     * @param subCodePart text to be transformed
     * @return hashCode in SHA-0 form
     */
    public String hashSHA0(String subCodePart) {
        return hash.hash("SHA1",subCodePart);
    }

    /**
     * Controller when the id used is SHA-256.
     * @param subCodePart text to be transformed
     * @return hashCode in SHA-256 form
     */
    public String hashSHA256(String subCodePart) {
        return hash.hash("SHA256",subCodePart);
    }

    /**
     * Method that returns a list with all the IDs.
     * @return a list with all the ids
     */
    public List<String> hashList() {
        List<String> ids = new ArrayList<>();
        ids.add("MD5");
        ids.add("SHA-0");
        ids.add("SHA-256");
        return ids;
    }
}
