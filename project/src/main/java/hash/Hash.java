package hash;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hash class: class that will make the calculations.
 */
public class Hash {

    /**
     * Constructor of the hash class.
     */
    public Hash(){
    }

    /**
     * Method that calculates the hash code.
     * @param id: MD5, SHA-0, SHA-256
     * @param text text that is supposed to be transformed
     * @return
     */
    public String hash(String id, String text){
        try {
            MessageDigest md = MessageDigest.getInstance(id);
            md.update(text.getBytes());
            byte[] digest = md.digest();
            String myHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();
            return myHash;
        }catch(NoSuchAlgorithmException e){
            e.printStackTrace();
            return "";
        }
    }

}
