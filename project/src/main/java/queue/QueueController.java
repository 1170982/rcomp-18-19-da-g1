package queue;

import java.net.InetAddress;
import java.util.*;

public class QueueController {

    /**
     * Identifier for this application
     */
    private static final String name = "Queue";

    /**
     * List of queues present in the application
     */
    private List<queue> listQueues;//list

    private HashSet<InetAddress> listIp; //talvez desnecessario

    /**
     * Controller creates a list of queues
     */
    public QueueController(){
        this.listQueues = new ArrayList<queue>();
        this.listIp = new HashSet(); //talvez desnecessario
    }

    /**
     * Stores text in queue relative to identifier, or creates a new one if identifier doesn't exist
     * @param identifier
     * @param text
     * @return size of queue
     */
    public int enqueue(String identifier, String text) {
        int size = 0;
        if(text.getBytes().length > 400){
            return -1; //Text cant be larger than 400 bytes
        }else {
            boolean queueExists = false;
            for (queue queueObject : this.listQueues) {
                if (queueObject.getIdentifier().equals(identifier)) {
                    size = queueObject.add(text);
                    queueExists = true;
                }
            }
            if (!queueExists) {//queue was not found, creates a new one
                //cannot be bigger than 80 characters
                if(identifier.length() > 80){
                    return -1;
                }
                queue newQueue = new queue(identifier);
                size = newQueue.add(text);
                listQueues.add(newQueue);
            }
        }
        return size;
    }

    /**
     * Removes last element to be added to the queue
     * @param identifier
     * @return Success or failure message
     */
    public String dequeue(String identifier) {
        for (queue queueObject : this.listQueues) {
            if (queueObject.getIdentifier().equals(identifier)) {
                return queueObject.dequeue();
            }
        }
        return "Queue with identifier does not exist !";
    }

    /**
     * Removes queue from the queue list
     * @param identifier
     * @return Success or failure message
     */
    public String remove(String identifier){
        boolean queueExists = false;
        queue queueFound = null;
        for (queue queueObject : this.listQueues) {
            if (queueObject.getIdentifier().equals(identifier)) {
                queueExists = true;
                queueFound = queueObject;
                break;
            }
        }
        if(!queueExists){
            return "\nQueue with identifier does not exist !";
        }else{
            listQueues.remove(queueFound);
        }
        return "Removed with success !";
    }

    /**
     * Returns a map with queue identifier and corresponding element size
     * @return map with identifier and element size
     */
    public Map<String, Integer> list(){
        Map<String, Integer> informationMap = new HashMap<String, Integer>();
        for (queue queueObject : this.listQueues) {
            informationMap.put(queueObject.getIdentifier(), queueObject.getSize());
        }
        return informationMap;
    }

}
