package queue;

import shared.Core;
import shared.RepeatedCalls;
import shared.UdpChatReceive;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class QueueUI {
    /**
     * Scanner object to allow user input to be interpreted
     */
    private static Scanner in = new Scanner(System.in);
    /**
     * Controller of this class
     */
    private QueueController queueController;

    /**
     * Public constructor
     */
    public QueueUI() {
        queueController = new QueueController();
        UdpChatReceive.setQueueController(queueController);
        System.out.println(" ### Queue Application ### ");
    }

    /**
     * Method that represents the user interface of this application
     * @param udpPacket DatagramPacket object, to allow messages to be sent
     * @throws Exception
     */
    public void presentation(DatagramPacket udpPacket) throws Exception {
        boolean continueUI = true;
        RepeatedCalls repeatedCalls = new RepeatedCalls("Queue|Start", udpPacket);
        repeatedCalls.canGo(true);
        repeatedCalls.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- Enqueue(identifier,text)");
            System.out.println("- Dequeue(identifier)");
            System.out.println("- Remove(identifier)");
            System.out.println("- List");
            System.out.println("Insert your code with the correct format: (Exit or 0 to End Application)");
            code = in.nextLine();

            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: "+entry.getKey() + ". IP Address: " + entry.getValue() + "\n");
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "Queue|End");
                continueUI = false;
            } else if (subCodeParts[0].equals("Hash") || subCodeParts[0].equals("NumConvert") || subCodeParts[0].equals("Stack") || subCodeParts[0].equals("TCP") || subCodeParts[0].equals("Var")) {
                System.out.println("In the wrong application, redirecting to the correct one...");
                //ChangeApp|Sentence
                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            } else if (!subCodeParts[0].equals("Queue")) {
                System.out.println("Application does not exist !");
            } else {
                try {
                    for (int i = 1; i < subCodeParts.length; i++) {
                        String command = subCodeParts[i];
                        //to find what is inside parenthesis
                        command = command.substring(command.indexOf("(") + 1);
                        command = command.substring(0, command.indexOf(")"));
                        try {
                            if (subCodeParts[1].contains("Enqueue")) {
                                String[] arguments = command.split(",");
                                int size = queueController.enqueue(arguments[0], arguments[1]);
                                if (size < 0) {
                                    System.out.println("\nEnqueue was not successful ! Text size cannot be bigger than 400 bytes and text identifier cannot have more than 80 characters !");
                                } else {
                                    System.out.println("\nEnqueue successful ! Size of queue: " + size);
                                }
                            } else if (subCodeParts[1].contains("Dequeue")) {
                                String response = queueController.dequeue(command);
                                if (response.equals("Queue with identifier does not exist !")) {
                                    System.out.println(response);
                                } else {
                                    System.out.println("\nElement with content: \"" + response + "\" was dequeued successfully !");
                                }
                            } else if (subCodeParts[1].contains("Remove")) {
                                String response = queueController.remove(command);
                                System.out.println("\n" + response);
                            } else if (subCodeParts[1].contains("List")) {
                                Map<String, Integer> informationMap = queueController.list();
                                System.out.println();
                                if (informationMap.size() == 0) {
                                    System.out.println("There are no indentifiers in the queue !");
                                } else {
                                    for (Map.Entry<String, Integer> entry : informationMap.entrySet()) {
                                        System.out.println("Identifier: " + entry.getKey() + ", Number of elements: " + entry.getValue());
                                    }
                                }
                                System.out.println();
                            } else {
                                System.out.println("Command does not match this application !");
                            }
                        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                            System.out.println("\nArguments are needed for this command !");
                        }
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println("You need to put parenthesis after each command !");
                }

            }
        }
        repeatedCalls.canGo(false);
        repeatedCalls.join();
    }
}