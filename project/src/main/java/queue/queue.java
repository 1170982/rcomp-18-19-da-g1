package queue;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class queue {
    /**
     * Queue attribute to hold the class' data
     */
    private Queue queueData;
    /**
     * String attribute to identify this queue
     */
    private String identifier;

    /**
     * Public constructor
     * @param identifier String that identifies this object
     */
    public queue(String identifier){
        this.identifier = identifier;
        this.queueData = new LinkedList();
    }

    /**
     * Returns identifier
     * @return
     */
    public String getIdentifier(){
        return this.identifier;
    }

    /**
     * Returns the number of elements in the queue
     * @return
     */
    public int getSize(){
        return queueData.size();
    }

    /**
     * Adds a string to the queue
     * @param text string
     * @return size of queue
     */
    public int add(String text) {
        queueData.add(text);
        return queueData.size();
    }

    /**
     * Removes the oldest element from the queue, or sends message of no elements existent in the queue
     * @return String removed
     */
    public String dequeue() {
        try {
            return (String)queueData.remove();
        } catch (NoSuchElementException e){
            return "There were no elements in the queue!";
        }
    }

}
