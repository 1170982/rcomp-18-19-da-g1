package stack;

import java.util.LinkedList;
import java.util.List;

public class StackController {

    /**
     * Stack list
     */
    private List<Stack> stackList;

    /**
     * Public constructor
     */
    public StackController() {
        stackList = new LinkedList<>();
    }

    /**
     * Pushes an element to a stack with a certain identifier. If it doesn't exist, it is created a new
     *
     * @param identifier identifier to be searched
     * @param text       texto to be added
     * @return size of the stack
     */
    public int push(String identifier, String text) {
        int i = 0;
        for (Stack stack : stackList) {
            if (stack.getIdentifier().equals(identifier)) {
                return stackList.get(i).pushSentence(text);
            }
            i++;
        }
        Stack newOne = new Stack(identifier);
        stackList.add(newOne);
        return stackList.get(i).pushSentence(text);
    }

    /**
     * Pushes an element from the stack with a certain identifier. If it doesn't exist, it is returned an error
     *
     * @param identifier identifier to be searched
     * @return element erased
     */
    public String pop(String identifier) {
        int i = 0;
        for (Stack stack : stackList) {
            if (stack.getIdentifier().equals(identifier)) {
                return stackList.get(i).popSentence();
            }
            i++;
        }
        return "Stack with given identifier does not exist!";
    }

    /**
     * List of all stacks
     *
     * @return list of stacks
     */
    public List<Stack> list() {
        return stackList;
    }
}

