package stack;

import shared.Core;
import shared.RepeatedCalls;
import shared.UdpChatReceive;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class StackUI {

    /**
     * Scanner object
     */
    private static Scanner in = new Scanner(System.in);

    /**
     * StackController object
     */
    private StackController stackController;

    /**
     * Public constructor
     */
    public StackUI() {
        stackController = new StackController();
        UdpChatReceive.setStackController(stackController);
        System.out.println(" ### Stack Application ### ");
    }

    /**
     * Class that shows the menu
     */
    public void presentation(DatagramPacket udpPacket) throws Exception {
        boolean continueUI = true;
        RepeatedCalls rc = new RepeatedCalls("Stack|Start", udpPacket);
        rc.canGo(true);
        rc.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- Stack|pushSentence(stack identifier,content)");
            System.out.println("- Stack|popSentence|(stack identifier)");
            System.out.println("- Stack|list()");
            System.out.println("Insert your code with the correct format: (0 to End Application)");
            code = in.nextLine();
            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: " + entry.getKey() + "\nIP Address: " + entry.getValue());
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "Stack|End");
                continueUI = false;
            } else if (!subCodeParts[0].equals("Stack")) {
                System.out.println("In the wrong application, redirecting to the correct one...");

                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            } else {
                try {
                    String command = subCodeParts[1];
                    //to find what is inside parenthesis
                    command = command.substring(command.indexOf("(") + 1);
                    command = command.substring(0, command.indexOf(")"));
                    String[] arguments = command.split(",");
                    if (subCodeParts[1].contains("pushSentence")) {
                        if (arguments[0].length() < 80 && arguments[1].length() < 400) {
                            int size = stackController.push(arguments[0], arguments[1]);
                            System.out.println("Push performed! Size of the stack: " + size);
                        } else {
                            System.out.println("Invalid sizes!");
                        }
                    } else if (subCodeParts[1].contains("popSentence")) {
                        String response = stackController.pop(arguments[0]);
                        if (response.equals("Stack with given identifier does not exist!")) {
                            System.out.println(response);
                        } else {
                            System.out.println("\nElement with content \"" + response + "\" was erased successfully!");
                        }
                    } else if (subCodeParts[1].contains("list")) {
                        List<Stack> identifiers = stackController.list();
                        System.out.println("Stacks in the system:");
                        for (Stack s : identifiers) {
                            System.out.println("-> " + s.getIdentifier() + "(Size: " + s.size() + ")");
                        }
                    } else {
                        System.out.println("Command does not match this application!");
                    }
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                    System.out.println("Arguments are needed for this command!");
                }
            }
        }
        rc.canGo(false);
        rc.join();
    }
}
