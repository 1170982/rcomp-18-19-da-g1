package stack;

public class Stack extends java.util.Stack<String> {

    /**
     * Stack identifier
     */
    private String identifier;

    /**
     * Constructor with identifier
     *
     * @param identifier identifier
     */
    public Stack(String identifier) {
        super();
        this.identifier = identifier;
    }

    /**
     * Obtain identifier
     *
     * @return identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Adds a new element to the stack
     *
     * @param phrase new element
     * @return size of the stack
     */
    public int pushSentence(String phrase) {
        super.push(phrase);
        return super.size();
    }

    /**
     * Pops the element of a stack
     *
     * @return removed element
     */
    public String popSentence() {
        return super.pop();
    }

}