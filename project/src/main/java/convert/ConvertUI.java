/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;

import shared.RepeatedCalls;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import shared.Core;
import shared.UdpChatReceive;

/**
 *
 * @author 1171138
 */
public class ConvertUI {

    private static Scanner in = new Scanner(System.in);

    private ConvertController convertController;

    public ConvertUI() {
        convertController = new ConvertController();
        UdpChatReceive.setConvertController(convertController);
        System.out.println(" ### Number Convert Application ### ");
    }

    public void presentation(DatagramPacket udpPacket) throws Exception {
        boolean continueUI = true;
        RepeatedCalls rc = new RepeatedCalls("NumConvert|Start", udpPacket);
        rc.canGo(true);
        rc.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- Convert(ir,or,number)");
            System.out.println("- List");
            System.out.println("Insert your code with the correct format: (0 to End Application)");
            code = in.nextLine();

            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: "+entry.getKey() + ". IP Address: " + entry.getValue() + "\n");
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "NumConvert|End");
                continueUI = false;
            }else if (!subCodeParts[0].equals("NumConvert") &&  !subCodeParts[0].equals("0")) {
                //reencaminhar para sistema
                System.out.println("In the wrong application, redirecting to the correct one...");
                //ChangeApp|Sentence
                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            } else {
                try {
                    for (int i = 1; i < subCodeParts.length; i++) {
                        String command = subCodeParts[i];
                        //to find what is inside parenthesis
                        command = command.substring(command.indexOf("(") + 1);
                        command = command.substring(0, command.indexOf(")"));
                        try {
                            if (subCodeParts[1].contains("Convert")) {
                                String[] arguments = command.split(",");

                                if (convertController.list().contains(arguments[0]) && convertController.list().contains(arguments[1])) {

                                    String numConvert = convertController.convert(arguments[0], arguments[1], arguments[2]);
                                    if (numConvert.contains("Error")) {
                                        System.out.println(numConvert);
                                    } else {
                                        System.out.println("Number in " + arguments[0] + ": " + arguments[2] + "\nNumber converted in " + arguments[1] + ": " + numConvert);
                                    }
                                } else {
                                    System.out.println("Invalid Identifiers");
                                }
                            } else if (subCodeParts[1].contains("List")) {
                                List<String> idList = convertController.list();
                                System.out.println("The identifiers for Number Convert App are:\n");
                                for (String s : idList) {
                                    System.out.println(s + "\n");
                                }
                                System.out.println();
                            } else {
                                System.out.println("Command does not match this application !");
                            }
                        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                            System.out.println("\nArguments are needed for this command !");
                        }
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println("You need to put parenthesis after each command !");
                }
            }
        }
        rc.canGo(false);
        rc.join();
    }

}
