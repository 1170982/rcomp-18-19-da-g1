/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author 1171138
 */
public class ConvertController {

    private int decimalValue;

    /**
     *  empty  convert controller contructor
     */
    public ConvertController() {

    }

    /**
     * Method that converts the number given by the user in the format ir to the
     * format or
     *
     * @param ir indentifier input (BIN, DEC, HEX, OCT)
     * @param or indentifier output (BIN, DEC, HEX, OCT)
     * @param number string with the number to be converted
     * @return stting with the number converted
     */
    public String convert(String ir, String or, String number) {
        if (ir.equals("BIN")) {
            try{
                decimalValue = Integer.parseInt(number, 2);
            }catch (IllegalArgumentException e) {
                return "Error on the number inserted, please insert the number in the right format";
            }

        } else if (ir.equals("OCT")) {
            try {
                decimalValue = Integer.parseInt(number, 8);
            } catch (IllegalArgumentException e) {
                return "Error on the number inserted, please insert the number in the right format";
            }
        } else if (ir.equals("HEX")) {
            try {
                decimalValue = Integer.parseInt(number, 16);
            } catch (IllegalArgumentException e) {
                return "Error on the number inserted, please insert the number in the right format";
            }
        } else {
            try {
                decimalValue = Integer.parseInt(number);
            } catch (IllegalArgumentException e) {
                return "Error on the number inserted, please insert the number in the right format";
            }
        }

        if (or.equals("BIN")) {
            return Integer.toBinaryString(decimalValue);
        } else if (or.equals("OCT")) {
            return Integer.toOctalString(decimalValue);
        } else if (or.equals("HEX")) {
            return Integer.toHexString(decimalValue);
        } else {
            return Integer.toString(decimalValue);
        }

    }

    /**
     * List of the indentifiers that can me used as ir and or
     */
    public List<String> list() {
        List<String> idList = new ArrayList<String>();
        idList.add("BIN");
        idList.add("OCT");
        idList.add("HEX");
        idList.add("DEC");
        return idList;
    }

}
