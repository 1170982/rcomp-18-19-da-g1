package TCPmonitor;

import java.io.*;
import java.net.*; 

class TCPserver {
	static ServerSocket sock;

	public static void startServer() throws Exception {
		Socket cliSock;

		try {
			sock = new ServerSocket(30101);
		}
		catch(IOException ex) {
			System.out.println("Failed to open server socket");
			System.exit(1);
		}
		try {
			while (true) {
				cliSock = sock.accept();
				new Thread(new TCPserverThread(cliSock)).start();
			}
		}catch (SocketTimeoutException e){
			System.out.println("Server Terminated");
		}
	}
}



class TCPserverThread implements Runnable {
	private Socket s;

	public TCPserverThread(Socket cli_s) { s=cli_s;}

	public void run() {
		InetAddress clientIP;

		clientIP=s.getInetAddress();
		System.out.println("New client connection from " + clientIP.getHostAddress() + 
			", port number " + s.getPort());
	}
}



