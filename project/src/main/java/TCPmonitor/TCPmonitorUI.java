package TCPmonitor;

import shared.Core;
import shared.RepeatedCalls;
import shared.UdpChatReceive;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class TCPmonitorUI {

    /**
     * Controller of the application
     */
    private TCPmonitorController tcpmc;

    /**
     * Public constructor
     */
    public TCPmonitorUI(){
        tcpmc = new TCPmonitorController();
        UdpChatReceive.setTcpController(tcpmc);
        System.out.println("TCP Monitor");
    }

    /**
     * Main UI method
     * @param udpPacket Communication to the core
     * @throws Exception
     */
    public void presentation(DatagramPacket udpPacket) throws Exception {
        Scanner sc = new Scanner(System.in);
        boolean continueUI = true;
        RepeatedCalls repeatedCalls = new RepeatedCalls("TCP|Start", udpPacket);
        repeatedCalls.canGo(true);
        repeatedCalls.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- TCP|addService(service identifier, ip, port number)");
            System.out.println("- TCP|removeService(service identifier)");
            System.out.println("- TCP|servicesStatus()");
            System.out.println("Insert your code with the correct format: (0 to End Application)");
            code = sc.nextLine();

            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: "+entry.getKey() + ". IP Address: " + entry.getValue() + "\n");
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "TCP|End");
                continueUI = false;
            }else if (subCodeParts[0].equals("Hash") || subCodeParts[0].equals("NumConvert") || subCodeParts[0].equals("Stack") || subCodeParts[0].equals("Queue") || subCodeParts[0].equals("Var")) {
                System.out.println("In the wrong application, redirecting to the correct one...");
                //ChangeApp|Sentence
                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            }else if (!subCodeParts[0].equals("TCP")) {
                System.out.println("Application does not exist !");
            } else {
                try {
                    for (int i = 1; i < subCodeParts.length; i++) {
                        String command = subCodeParts[i];
                        //to find what is inside parenthesis
                        command = command.substring(command.indexOf("(") + 1);
                        command = command.substring(0, command.indexOf(")"));
                        try {
                            String[] arguments = command.split(",");
                            if (subCodeParts[1].contains("addService")) {
                                int portNumber;
                                try{
                                    portNumber=Integer.parseInt(arguments[2]);
                                }catch(IllegalArgumentException e){
                                    System.out.println("Illegal port number!");
                                    break;
                                }
                                if(tcpmc.addService(arguments[0], arguments[1], portNumber))
                                    System.out.println("Service successfully added to the application!");
                                else
                                    System.out.println("Error adding the service to the application!");
                            } else if (subCodeParts[1].contains("removeService")) {
                                if(tcpmc.removeService(arguments[0]))
                                    System.out.println("Service successfully removed from the application!");
                                else
                                    System.out.println("Error removing the service from the application!");
                            } else if (subCodeParts[1].contains("servicesStatus")) {
                                List<String> services = tcpmc.servicesStatus();
                                for(String str : services)
                                    System.out.println(str);
                                System.out.println("\nServices outputted successfully!");
                            } else {
                                System.out.println("Command does not match this application!");
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.out.println("Arguments are needed for this command!");
                        }
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println("You need to put parenthesis after each command!");
                }
            }
        }
        repeatedCalls.canGo(false);
        repeatedCalls.join();
    }
}
