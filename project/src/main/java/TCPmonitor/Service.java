package TCPmonitor;

public class Service {

    /**
     * Possible status
     */
    private enum possibleStatus{
        UNCHECKED, AVAILABLE, UNAVAILABLE
    }

    /**
     * Identifier of the Service
     */
    private String id;

    /**
     * IP Address of the Service
     */
    private String ip;

    /**
     * TCP Port Number of the Service
     */
    private int tcpPortNumber;

    /**
     * Status of the Service
     */
    private possibleStatus status;

    /**
     * Public constructor
     *
     * @param id1 identifier
     * @param ip1 IP address
     * @param tcpPortNumber1 tcp port number
     */
    public Service(String id1, String ip1, int tcpPortNumber1){
        id=id1;
        ip=ip1;
        tcpPortNumber=tcpPortNumber1;
        status=possibleStatus.UNCHECKED;
    }

    /**
     * Method that returns the id of the service
     *
     * @return id
     */
    public String getId(){
        return this.id;
    }

    /**
     * Method that returns the tcp port number of the service
     *
     * @return tcp port number
     */
    public int getTcpPortNumber(){
        return this.tcpPortNumber;
    }

    /**
     * Method that returns the status of the service
     *
     * @return status
     */
    public String getStatus(){
        return this.status.name();
    }

    /**
     * Method to update the status of the service
     *
     * @param st new status
     * @return success of the operation
     */
    public boolean setStatus(String st){
        for(possibleStatus ps : possibleStatus.values()){
            String str = ps.name();
            if(str.equalsIgnoreCase(st)) {
                status = ps;
                return true;
            }
        }
        return false;
    }

    /**
     * Equals method
     *
     * @param obj Object to compare to
     * @return true if equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Service ss = (Service) obj;
        return ss.id.equals(this.id);
    }
}
