package TCPmonitor;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class TCPmonitorController {

    /**
     * List of added services
     */
    private List<Service> services;

    /**
     * IP address of the TCP Server
     */
    private String serverIPadd;

    /**
     * Controller of the application
     */
    public TCPmonitorController () {
        Thread thr;
        try {
            thr = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        TCPserver.startServer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thr.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            serverIPadd = socket.getLocalAddress().getHostAddress();
        } catch (SocketException | UnknownHostException e) {
            System.out.println("Error getting server IP address.");
        }
        services = new ArrayList<>();
    }

    /**
     * Method that returns the list of services
     *
     * @return list of Services
     */
    public List<Service> getServices(){
        return new ArrayList<>(services);
    }

    /**
     * Method that given the parameters creates a new Service
     *
     * @param id service id
     * @param ip service ip address
     * @param tcpPortNumber service tcp port number (used in connection)
     *
     * @return success of the operation
     */
    public boolean addService(String id, String ip, int tcpPortNumber){
        Service ss = new Service(id, ip, tcpPortNumber);
        Service tmp = null;
        for(Service serv : services)
            if(serv.equals(ss))
                tmp = serv;
        if(tmp != null)
            services.remove(tmp);
        services.add(ss);
        return true;
    }

    /**
     * Method that removes a service from the application
     *
     * @param id id of the service to remove
     * @return success of the operation
     */
    public boolean removeService(String id){
        Service tmp = null;
        for(Service ss : services)
            if(ss.getId().equals(id))
                tmp = ss;
        if (tmp != null) {
            services.remove(tmp);
            return true;
        }
        return false;
    }

    /**
     * Method that checks if the existant services are available (successfully connected to the server) or unavailable
     *
     * @return List of services and status
     */
    public List<String> servicesStatus(){
        List<String> servicesFinal = new ArrayList<>();
        InetAddress serverIP=null;
        Socket sock;

        try {
            serverIP = InetAddress.getByName(serverIPadd);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid server specified: ");
            System.exit(1);
        }

        for(Service ss:services) {
            int flag=0;
            StringBuilder sb = new StringBuilder();
            sb.append(ss.getId()).append("; ");
            try {
                sock = new Socket(serverIP, ss.getTcpPortNumber());
                sock.setSoTimeout(5000);
            } catch (IOException ex) {
                flag=1;
            }
            if(flag==0) {
                System.out.println("Connection Established!");
                ss.setStatus("available");
            }else{
                System.out.println("Failed to establish TCP connection");
                ss.setStatus("unavailable");
            }
            sb.append(ss.getStatus());
            servicesFinal.add(sb.toString());
        }
        return servicesFinal;
    }
}
