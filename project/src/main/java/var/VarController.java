package var;

import java.util.ArrayList;
import java.util.List;

public class VarController {

    /**
     * List of var objects
     */
    private List<Var> listVar;

    /**
     * Controller that creates a list of Var objects
     */
    public VarController() {
        this.listVar = new ArrayList<>();
    }

    /**
     * Stores text in existent var relative to a given identifier or creates a new one if identifier doesn't exist
     *
     * @param identifier - var identifier
     * @param text       - var text
     * @return var data information
     */
    public String store(String identifier, String text) {
        String information = "";
        if (text.getBytes().length > 400) {
            return ""; //Text cant be larger than 400 bytes
        } else if (text.length() == 0) { //Content is empty, removes from list
            Var tempVar = null;
            for (Var var : this.listVar) {
                if (var.getIdentifier().equals(identifier)) {
                    tempVar = var;
                    information = "*";
                }
            }
            listVar.remove(tempVar);
        } else {
            boolean varExists = false;
            for (Var var : this.listVar) {
                if (var.getIdentifier().equals(identifier)) {
                    //Overwrites if it already exists
                    information = var.store(text);
                    varExists = true;
                }
            }
            if (!varExists) {//var was not found, creates a new one
                //cannot be bigger than 80 characters
                if (identifier.length() > 80) {
                    return "";
                }
                Var newVar = new Var(identifier);
                information = newVar.store(text);
                listVar.add(newVar);
            }
        }
        return information;
    }

    /**
     * Method that return the Var data information. If the var does not exist show a failure message.
     *
     * @param identifier - var identifier
     * @return Success or failure message
     */
    public String fetch(String identifier) {
        String information;
        for (Var var : this.listVar) {
            if (var.getIdentifier().equals(identifier)) {
                //Overwrites if it already exists
                information = var.fetch();
                return information;
            }
        }
        return "\nVar does not exist!";
    }

    /**
     * Method that returns a list with var identifier
     *
     * @return map with identifier and element size
     */
    public List<String> list() {
        List<String> identifierList = new ArrayList<>();
        int byteSize = 0;
        for (Var var : this.listVar) {
            identifierList.add(var.getIdentifier());
            byteSize += var.getIdentifier().getBytes().length;
            if(byteSize > 400){
                identifierList.remove(var.getIdentifier());
                break;
            }
        }
        return identifierList;
    }

    /**
     * Removes var from the var list
     *
     * @param identifier - var identifier
     * @return Success or failure message
     */
    public String erase(String identifier) {
        boolean varExists = false;
        Var varFound = null;
        for (Var var : this.listVar) {
            if (var.getIdentifier().equals(identifier)) {
                varExists = true;
                varFound = var;
                break;
            }
        }
        if (!varExists) {
            return "\nVar with identifier does not exist!";
        } else {
            String content = varFound.fetch();
            listVar.remove(varFound);
            return content;
        }
    }
}
