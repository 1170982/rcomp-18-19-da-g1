package var;

public class Var {

    /**
     * Var data information
     */
    private String varData;

    /**
     * Var identifier
     */
    private String identifier;

    /**
     * Constructor with the identifier
     *
     * @param - var identifier
     */
    public Var(String identifier) {
        this.identifier = identifier;
        this.varData = "";
    }

    /**
     * Method that return the Var identifier
     *
     * @return - var identifier
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * Method that return the Var data information
     *
     * @return - var data information
     */
    public String fetch() {
        return varData;
    }

    /**
     * Method that stores the Var data information and returns it
     *
     * @param text - var data information to add
     * @return - var data information
     */
    public String store(String text) {
        this.varData = text;
        return text;
    }

}

