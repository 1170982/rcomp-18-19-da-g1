package var;

import shared.Core;
import shared.RepeatedCalls;
import shared.UdpChatReceive;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class VarUI {

    /**
     * Scanner object
     */
    private static Scanner in = new Scanner(System.in);

    /**
     * VarController object
     */
    private VarController varController;

    /**
     * Public constructor
     */
    public VarUI() {
        varController = new VarController();
        UdpChatReceive.setVarController(varController);
        System.out.println(" ### Var Application ### ");
    }

    /**
     * Method that shows the menu
     *
     * @param udpPacket
     * @throws Exception
     */
    public void presentation(DatagramPacket udpPacket) throws Exception {
        boolean continueUI = true;
        RepeatedCalls rc = new RepeatedCalls("Var|Start", udpPacket);
        rc.canGo(true);
        rc.start();
        while (continueUI) {
            String code = "";
            System.out.println("\nFollowing codes are available: ");
            System.out.println("- Store(identifier,text)");
            System.out.println("- Fetch(identifier)");
            System.out.println("- List()");
            System.out.println("- Erase(identifier)");
            System.out.println("Insert your code with the correct format: (Exit or 0 to End Application)");
            code = in.nextLine();

            String[] subCodeParts = code.split("\\|");
            if (subCodeParts[0].equals("Status")) {
                for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                    System.out.println("Application type: " + entry.getKey() + ". IP Address: " + entry.getValue() + "\n");
                }
            } else if (subCodeParts[0].equals("Exit") || subCodeParts[0].equals("0")) {
                //sends end message
                Core.message(udpPacket, "Var|End");
                continueUI = false;
            } else if (subCodeParts[0].equals("Hash") || subCodeParts[0].equals("NumConvert") || subCodeParts[0].equals("Stack") || subCodeParts[0].equals("TcpMonitor") || subCodeParts[0].equals("Queue")) {
                System.out.println("In the wrong application, redirecting to the correct one...");
                //ChangeApp|Sentence
                DatagramSocket socket = new DatagramSocket();
                String serverIPadd = "";
                try {
                    socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                    serverIPadd = socket.getLocalAddress().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                Core.message(udpPacket, serverIPadd + "|ChangeApp|" + code);
                TimeUnit.SECONDS.sleep(1);
            } else if (!subCodeParts[0].equals("Var")) {
                System.out.println("Application does not exist!");
            } else {
                try {
                    for (int i = 1; i < subCodeParts.length; i++) {
                        String command = subCodeParts[i];
                        //to find what is inside parenthesis
                        command = command.substring(command.indexOf("(") + 1);
                        command = command.substring(0, command.indexOf(")"));
                        try {
                            if (subCodeParts[1].contains("store")) {
                                String[] arguments = command.split(",");
                                String information = varController.store(arguments[0], arguments[1]);
                                if (information.equals("*")) {
                                    System.out.println("\nContent was empty! Identifier was removed!");
                                } else if (information.length() == 0) {
                                    System.out.println("\nStore was not successful! Text size cannot be bigger than 400 bytes and text identifier cannot have more than 80 characters!");
                                } else {
                                    System.out.println("\nStore successful with the content: " + information);
                                }
                            } else if (subCodeParts[1].contains("fetch")) {
                                String response = varController.fetch(command);
                                if (response.equals("\nVar does not exist!")) {
                                    System.out.println(response);
                                } else {
                                    System.out.println("\nElement with identifier: \"" + command + "\" has the following content: " + response);
                                }
                            } else if (subCodeParts[1].contains("list")) {
                                List<String> listIdentifiers = varController.list();
                                System.out.println();
                                if (listIdentifiers.size() == 0) {
                                    System.out.println("There are no elements!");
                                } else {
                                    for (String identifier : listIdentifiers) {
                                        System.out.println("Identifier: " + identifier);
                                    }
                                }
                            } else if (subCodeParts[1].contains("erase")) {
                                String content = varController.erase(command);
                                if (content.equals("\nVar with identifier does not exist!")) {
                                    System.out.println("\nVar with identifier does not exist!");
                                } else {
                                    System.out.println("\nElement with content: \"" + content + "\" was erased!");
                                }
                            } else {
                                System.out.println("Command does not match this application!");
                            }
                        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                            System.out.println("\nArguments are needed for this command!");
                        }
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println("You need to put parenthesis after each command!");
                }
            }
        }
        rc.canGo(false);
        rc.join();
    }

}
