package shared;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;

public class MessageFetcher extends Thread {
    private Socket TCPconn;
    private InetAddress serverIP;
    private int serverPort;
    private int nextMessage;
    private DatagramPacket udp;

    public MessageFetcher(InetAddress ip, int port, DatagramPacket udp) {
        serverIP = ip;
        serverPort = port;
        this.udp = udp;
    }

    public void run() {
        nextMessage = 0;
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        String serverIPadd = "";
        try {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            serverIPadd = socket.getLocalAddress().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        while (Core.continueApp) {
            String message = getNextMessage();
            if (!Core.continueApp) return;
            if (message == null) {
                nextMessage = 0;
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ie) {
                    System.out.println("Thread Interrupted ...");
                }
            } else {
                System.out.println(message);
                int out = 0;
                boolean first = true;
                for (int i = 0; i < message.length(); i++) {
                    if (String.valueOf(message.charAt(i)).equals(")") && first) {
                        out = i + 1;
                        first = false;
                    }
                }
                String finalMessage = message.substring(out).trim();
                nextMessage++;
                try {
                    Core.message(udp, serverIPadd + "|ChangeApp|" + finalMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private String getNextMessage() {
        DataOutputStream sOut;
        DataInputStream sIn;
        try {
            synchronized (serverIP) {
                TCPconn = new Socket(serverIP, serverPort);
            }
        } catch (IOException ex) {
            synchronized (serverIP) {
                TCPconn = null;
            }
            return null;
        }

        HTTPmessage response;
        try {
            sOut = new DataOutputStream(TCPconn.getOutputStream());
            sIn = new DataInputStream(TCPconn.getInputStream());
            HTTPmessage request = new HTTPmessage();
            request.setRequestMethod("GET");
            request.setURI("/messages/" + nextMessage);
            request.send(sOut);
            response = new HTTPmessage(sIn);    // the server may hold the response
            if (!response.getStatus().startsWith("200")) {
                throw new IOException();
            }

        } catch (IOException ex) {
            synchronized (serverIP) {
                try {
                    TCPconn.close();
                } catch (IOException ex2) {
                }
                TCPconn = null;
            }
            return null;
        }
        synchronized (serverIP) {
            try {
                TCPconn.close();
            } catch (IOException ex2) {
            }
            TCPconn = null;
        }
        return (new String(response.getContent()));
    }

    public void abort() { // close the socket to force the thread's exit
        synchronized (serverIP) {
            if (TCPconn == null) return;
            try {
                TCPconn.close();
            } catch (IOException ex2) {
            }
        }
    }

} // MessageFetcher CLASS





