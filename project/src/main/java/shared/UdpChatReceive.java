package shared;

import TCPmonitor.TCPmonitorController;
import convert.ConvertController;
import hash.HashController;
import queue.QueueController;
import stack.Stack;
import stack.StackController;
import var.VarController;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;

/**
 * Class responsible for receiving messages from the network
 */
public class UdpChatReceive implements Runnable {

    private static InetAddress serverIP;
    private static int serverPort;

    /**
     * HashController object
     */
    private static HashController hashController;

    /**
     * StackController object
     */
    private static StackController stackController;

    /**
     * QueueController object
     */
    private static QueueController queueController;

    /**
     * VarController object
     */
    private static VarController varController;

    /**
     * ConvertController object
     */
    private static ConvertController conController;

    /**
     * TCPmonitorController object
     */
    private static TCPmonitorController tcpController;
    /**
     * Map with active application names and the currently IP
     */
    private static HashMap<String, InetAddress> listApplicationsIp = new HashMap<>();
    /**
     * DatagramSocket object
     */
    private DatagramSocket s;
    /**
     * DatagramPacket object
     */
    private DatagramPacket packet;

    private String[] args;

    /**
     * Public constructor
     *
     * @param udp_s DatagramSocket
     * @param p     DatagramPacket
     */
    public UdpChatReceive(DatagramSocket udp_s, DatagramPacket p, String[] args) {
        s = udp_s;
        this.packet = p;
        this.args = args;
    }

    /**
     * Method that returns the map of active applications
     *
     * @return map of active application
     */
    public static HashMap<String, InetAddress> getListApplicationsIp() {
        return listApplicationsIp;
    }

    /**
     * Method that sets the queue controller. Used by the UI to make sure the application uses the same controller for network and local requests
     *
     * @param queueControllerC queueController to be used
     */
    public static void setQueueController(QueueController queueControllerC) {
        queueController = queueControllerC;
    }

    /**
     * Method that sets the stack controller. Used by the UI to make sure the application uses the same controller for network and local requests
     *
     * @param stackControllerC stackController to be used
     */
    public static void setStackController(StackController stackControllerC) {
        stackController = stackControllerC;
    }

    public static void setConvertController(ConvertController convController) {
        conController = convController;
    }

    /**
     * Method that sets the VAR controller. Used by the UI to make sure the application uses the same controller for network and local requests
     *
     * @param varControllerC varController to be used
     */
    public static void setVarController(VarController varControllerC) {
        varController = varControllerC;
    }

    /**
     * Method that sets the TCPMonitor controller. Used by the UI to make sure the application uses the same controller for network and local requests
     *
     * @param tcpController1 queueController to be used
     */
    public static void setTcpController(TCPmonitorController tcpController1) {
        tcpController = tcpController1;
    }

    /**
     * Method that sets the Hash controller. Used by the UI to make sure the application uses the same controller for network and local requests
     *
     * @param hashControllers queueController to be used
     */
    public static void setHashController(HashController hashControllers) {
        hashController = hashControllers;
    }

    /**
     * Run method of the receiver, that handles the outside messages that the system receives
     */
    public void run() {
        byte[] data = new byte[300];
        String phrase;
        DatagramPacket p;

        p = new DatagramPacket(data, data.length);

        try {
            serverIP = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid SERVER-ADDRESS: " + args[0]);
            System.exit(1);
        }

        try {
            serverPort = parseInt(args[1]);
        } catch (NumberFormatException ex) {
            System.out.println("Invalid SERVER-PORT-NUMBER: " + args[1]);
            System.exit(1);
        }

        MessageFetcher msgF = new MessageFetcher(serverIP, serverPort, packet);
        msgF.start();
        String textLine = "";
        //Always checking if new messages exists
        while (Core.continueApp) {
            p.setLength(data.length);
            try {
                s.receive(p);
            } catch (IOException ex) {
                return;
            }
            if (data[0] == 1) { // peer start
                Core.addIP(p.getAddress());
                try {
                    s.send(p);
                } catch (IOException ex) {
                    return;
                }
            } else if (data[0] == 0) { // peer exit
                Core.remIP(p.getAddress());
            } else {
                try {
                    //Message received by the system
                    phrase = new String(p.getData(), 0, p.getLength());
                    String[] subparts = phrase.split("\\|");
                    if (subparts.length > 1 && Core.continueApp) {
                        //Method that checks if the system will run or not the command in the message
                        if (subparts[1].equals("ChangeApp")) {
                            applicationRedirection(phrase); //choose application to put with ID on map, and runs it
                        }
                        //Method that prints the message sent by another
                        if (subparts[0].equals("Execute")) {
                            String sentence = "";
                            for (int i = 1; i < subparts.length; i++) {
                                sentence += subparts[i];
                                if (i == 1 && subparts.length > 2) {
                                    sentence += "|";
                                }
                            }
                            if (!postMessage(sentence)) {
                                System.out.println("Message not printed:\n" + sentence);
                            }
                        }

                        String applicationName = subparts[0]; //choose application to put with ID on map, and runs it

                        if (subparts.length == 2) {
                            //Puts on IP list
                            if (subparts[1].equals("Start")) {
                                listApplicationsIp.put(applicationName, p.getAddress());
                            }
                            //Removes of the IP list
                            if (subparts[1].equals("End")) {
                                listApplicationsIp.remove(applicationName);
                            }
                        }
                    }
                } catch (Exception ignored) {
                    //All the errors are ignored
                }
            }
        }
        try {
            Core.message(packet, "Exit");
            msgF.abort();
            msgF.join();
        } catch (Exception e) {
            //Application 1: Exception java.net.SocketException is natural, since the server is not running
        }
    }

    /**
     * Method that posts a message on the HTTP server
     *
     * @param msg message to be posted
     * @return if the message was published or not
     */
    public boolean postMessage(String msg) {
        Socket TCPconn;
        DataOutputStream sOut;
        DataInputStream sIn;
        try {
            TCPconn = new Socket(serverIP, serverPort);
        } catch (IOException ex) {
            return false;
        }

        try {
            sOut = new DataOutputStream(TCPconn.getOutputStream());
            sIn = new DataInputStream(TCPconn.getInputStream());
            HTTPmessage request = new HTTPmessage();
            request.setRequestMethod("POST");
            request.setURI("/messages");
            request.setContentFromString(msg, "text/plain");
            request.send(sOut);
            HTTPmessage response = new HTTPmessage(sIn);
            if (!response.getStatus().startsWith("200")) {
                throw new IOException();
            }

        } catch (IOException ex) {
            try {
                TCPconn.close();
            } catch (IOException ex2) {
            }
            return false;
        }
        try {
            TCPconn.close();
        } catch (IOException ex2) {
        }
        return true;
    }

    /**
     * Method that handles messages from the outside when they have the 'ChangeApp' instruction on it.
     * The program also checks if the current program is the required (if it is not, the method ends) and even if the requested program is online.
     *
     * @param phrase Message sent by the outside
     * @throws Exception multiple exceptions
     */
    private void applicationRedirection(String phrase) throws Exception {
        //The following lines are to obtain the current system IP, to check if the request sender and receiver are not the same
        DatagramSocket socket = new DatagramSocket();
        socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
        String serverIPadd = socket.getLocalAddress().getHostAddress();
        String[] applicationName = phrase.split("\\|");
        String[] ipDivided = applicationName[0].split("\\.");
        byte[] ipAddr = new byte[]{(byte) Integer.parseInt(ipDivided[0]), (byte) Integer.parseInt(ipDivided[1]), (byte) Integer.parseInt(ipDivided[2]), (byte) Integer.parseInt(ipDivided[3])};
        InetAddress ida = InetAddress.getByAddress(ipAddr);
        boolean selfProgram = false;
        if (applicationName[2].equals("Status")) {
            if (getListApplicationsIp().isEmpty()) {
                Core.messageTo(packet, "Execute|There are not active applications.", ida);
            }
            for (Map.Entry<String, InetAddress> entry : UdpChatReceive.getListApplicationsIp().entrySet()) {
                Core.messageTo(packet, "Execute|Application type: " + entry.getKey() + "\nIP Address: " + entry.getValue() + "\n", ida);
            }
        } else if (applicationName[2].equals("Exit")) {
            Core.continueApp = false;

        } else {
            String command = applicationName[3];
            //to find what is inside parenthesis
            //not necessary with hash, since it has another command syntax
            if (!applicationName[2].equals("Hash")) {
                command = command.substring(command.indexOf("(") + 1);
                command = command.substring(0, command.indexOf(")"));
            }
            String[] arguments = command.split(",");
            //The system checks if the application requested is running
            if (listApplicationsIp.keySet().contains(applicationName[2])) {
                //and if it is not the same as the requesting
                if (!listApplicationsIp.get(applicationName[2]).getHostAddress().equals(serverIPadd)) {
                    selfProgram = true;
                }
                //If the application is open and the requester is not on this phase, the program runs
                if (listApplicationsIp.keySet().contains(applicationName[2]) && !selfProgram) {
                    if (applicationName[2].equals("Stack")) {
                        if (applicationName[3].contains("help")) {
                            Core.messageTo(packet, "Execute|Following codes are available: ", ida);
                            Core.messageTo(packet, "Execute|- Stack|pushSentence(stack identifier,content)", ida);
                            Core.messageTo(packet, "Execute|- Stack|popSentence|(stack identifier)", ida);
                            Core.messageTo(packet, "Execute|- Stack|list()", ida);
                            Core.messageTo(packet, "Execute|Insert your code with the correct format: (0 to End Application)", ida);
                        } else if (applicationName[3].contains("pushSentence") && arguments.length == 2) {
                            if (arguments[0].length() < 80 && arguments[1].length() < 400) {
                                int size = stackController.push(arguments[0], arguments[1]);
                                Core.messageTo(packet, "Execute|Push performed! Size of the stack: " + size, ida);
                            } else {
                                Core.messageTo(packet, "Execute|Invalid sizes!", ida);
                            }
                        } else if (applicationName[3].contains("popSentence")) {
                            String response = stackController.pop(arguments[0]);
                            if (response.equals("Execute|Stack with given identifier does not exist!")) {
                                Core.messageTo(packet, response, ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nElement with content \"" + response + "\" was erased successfully!", ida);
                            }
                        } else if (applicationName[3].contains("list")) {
                            List<Stack> identifiers = stackController.list();
                            Core.messageTo(packet, "Execute|Stacks in the system:", ida);
                            for (Stack s : identifiers) {
                                Core.messageTo(packet, "Execute|-> " + s.getIdentifier() + "(Size: " + s.size() + ")", ida);
                            }
                        } else {
                            Core.messageTo(packet, "Execute|Command does not match this application!", ida);
                        }
                    }

                    if (applicationName[2].equals("Queue")) {
                        if (applicationName[3].contains("Enqueue")) {
                            int size = queueController.enqueue(arguments[0], arguments[1]);
                            if (size < 0) {
                                Core.messageTo(packet, "Execute|\nEnqueue was not successful ! Text size cannot be bigger than 400 bytes and text identifier cannot have more than 80 characters !", ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nEnqueue successful ! Size of queue: " + size, ida);
                            }
                        } else if (applicationName[3].contains("Dequeue")) {
                            String response = queueController.dequeue(command);
                            if (response.equals("Queue with identifier does not exist !")) {
                                Core.messageTo(packet, "Execute|" + response, ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nElement with content: \"" + response + "\" was dequeued successfully !", ida);
                            }
                        } else if (applicationName[3].contains("Remove")) {
                            Core.messageTo(packet, queueController.remove(command), ida);
                        } else if (applicationName[3].contains("List")) {
                            Map<String, Integer> informationMap = queueController.list();
                            if (informationMap.size() == 0) {
                                Core.messageTo(packet, "Execute|There are no indentifiers in the queue !", ida);
                            } else {
                                for (Map.Entry<String, Integer> entryMap : informationMap.entrySet()) {
                                    Core.messageTo(packet, "Execute|Identifier: " + entryMap.getKey() + ", Number of elements: " + entryMap.getValue(), ida);
                                }
                            }
                        } else if (applicationName[3].equals("help")) {
                            Core.messageTo(packet, "Execute|\nFollowing codes are available: ", ida);
                            Core.messageTo(packet, "Execute|- Enqueue(identifier,text)", ida);
                            Core.messageTo(packet, "Execute|- Dequeue(identifier)", ida);
                            Core.messageTo(packet, "Execute|- Remove(identifier)", ida);
                            Core.messageTo(packet, "Execute|- List()", ida);
                            Core.messageTo(packet, "Execute|Insert your code with the correct format: (0 or Exit to End Application)", ida);
                        } else {
                            Core.messageTo(packet, "Execute|Command does not match this application!", ida);
                        }
                    }
                    if (applicationName[2].equals("Var")) {
                        if (applicationName[3].contains("Store")) {
                            String information = varController.store(arguments[0], arguments[1]);
                            if (information.equals("*")) {
                                Core.messageTo(packet, "Execute|\nContent was empty ! Identifier was removed !", ida);

                            } else if (information.length() == 0) {
                                Core.messageTo(packet, "Execute|\nStore was not successful ! Text size cannot be bigger than 400 bytes and text identifier cannot have more than 80 characters !", ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nStore successful with the content: " + information, ida);
                            }
                        } else if (applicationName[3].contains("Fetch")) {
                            String response = varController.fetch(command);
                            if (response.equals("\nVar does not exist !")) {
                                Core.messageTo(packet, "Execute|" + response, ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nElement with identifier: \"" + command + "\" has the following content: " + response, ida);
                            }
                        } else if (applicationName[3].contains("List")) {
                            List<String> listIdentifiers = varController.list();
                            if (listIdentifiers.size() == 0) {
                                Core.messageTo(packet, "Execute|There are no elements !", ida);
                            } else {
                                for (String identifier : listIdentifiers) {
                                    Core.messageTo(packet, "Execute|Identifier: " + identifier, ida);
                                }
                            }
                        } else if (applicationName[3].contains("Erase")) {
                            String content = varController.erase(command);
                            if (content.equals("Execute|\nVar with identifier does not exist !")) {
                                Core.messageTo(packet, "Execute|\nVar with identifier does not exist !", ida);
                            } else {
                                Core.messageTo(packet, "Execute|\nElement with content: \"" + content + "\" was erased !", ida);
                            }
                        } else if (applicationName[3].equals("help")) {
                            Core.messageTo(packet, "Execute|\nFollowing codes are available: ", ida);
                            Core.messageTo(packet, "Execute|- Store(identifier,text)", ida);
                            Core.messageTo(packet, "Execute|- Fetch(identifier)", ida);
                            Core.messageTo(packet, "Execute|- List", ida);
                            Core.messageTo(packet, "Execute|- Erase(identifier)", ida);
                            Core.messageTo(packet, "Execute|Insert your code with the correct format: (0 or Exit to End Application)", ida);
                        } else {
                            Core.messageTo(packet, "Execute|Command does not match this application!", ida);
                        }
                    }

                    if (applicationName[2].equals("NumConvert")) {
                        if (applicationName[3].contains("Convert")) {
                            if (conController.list().contains(arguments[0]) && conController.list().contains(arguments[1])) {
                                String numConvert = conController.convert(arguments[0], arguments[1], arguments[2]);
                                if (numConvert.contains("Error")) {
                                    Core.messageTo(packet, numConvert, ida);
                                } else {
                                    Core.messageTo(packet, "Execute|Number in " + arguments[0] + ": " + arguments[2] + "\nNumber converted in " + arguments[1] + ": " + numConvert, ida);
                                }
                            } else {
                                Core.messageTo(packet, "Execute|Invalid Identifiers", ida);
                            }
                        } else if (applicationName[3].contains("List")) {
                            List<String> idList = conController.list();
                            Core.messageTo(packet, "Execute|The identifiers for Number Convert App are:\n", ida);
                            for (String s : idList) {
                                Core.messageTo(packet, "Execute|" + s + "\n", ida);
                            }

                        } else if (applicationName[3].contains("help")) {
                            Core.messageTo(packet, "Execute|\nFollowing codes are available: ", ida);
                            Core.messageTo(packet, "Execute|- Convert(ir,or,number)", ida);
                            Core.messageTo(packet, "Execute|- List", ida);
                            Core.messageTo(packet, "Execute|Insert your code with the correct format: (0 to End Application)", ida);
                        } else {
                            Core.messageTo(packet, "Execute|Command does not match this application !", ida);
                        }
                    }

                    if (applicationName[2].equals("TCP")) {
                        try {
                            if (applicationName[3].contains("addService")) {
                                int portNumber = 0;
                                try {
                                    portNumber = Integer.parseInt(arguments[2]);
                                } catch (IllegalArgumentException e) {
                                    Core.messageTo(packet, "Execute|Illegal port number!", ida);
                                }
                                if (tcpController.addService(arguments[0], arguments[1], portNumber))
                                    Core.messageTo(packet, "Execute|Service successfully added to the application!", ida);
                                else
                                    Core.messageTo(packet, "Execute|Error adding the service to the application!", ida);
                            } else if (applicationName[3].contains("removeService")) {
                                if (tcpController.removeService(arguments[0]))
                                    Core.messageTo(packet, "Execute|Service successfully removed from the application!", ida);
                                else
                                    Core.messageTo(packet, "Execute|Error removing the service from the application!", ida);
                            } else if (applicationName[3].contains("servicesStatus")) {
                                List<String> services = tcpController.servicesStatus();
                                if (services.isEmpty())
                                    Core.messageTo(packet, "Execute|No services exist in the application!", ida);
                                for (String str : services)
                                    Core.messageTo(packet, "Execute|" + str, ida);
                                Core.messageTo(packet, "Execute|\nServices outputted successfully!", ida);
                            } else if (applicationName[3].contains("help")){
                                Core.messageTo(packet, "Execute|\nFollowing codes are available: ", ida);
                                Core.messageTo(packet, "Execute|- TCP|addService(service identifier, ip, port number)", ida);
                                Core.messageTo(packet, "Execute|- TCP|removeService(service identifier)", ida);
                                Core.messageTo(packet, "Execute|- TCP|servicesStatus()", ida);
                            } else {
                                Core.messageTo(packet, "Execute|Command does not match this application!", ida);
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            Core.messageTo(packet, "Execute|You need to put parenthesis after each command!", ida);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            Core.messageTo(packet, "Execute|Arguments are needed for this command!", ida);
                        }
                    }
                    if (applicationName[2].equals("Hash")) {
                        if (applicationName.length == 6) {
                            if (applicationName[3].equals("Digest") && applicationName[4].equals("MD5")) {
                                String hashC = hashController.hashMD5(applicationName[5]);
                                Core.messageTo(packet, "Execute|The hash code in MD5 is: " + hashC + ".", ida);
                            } else if (applicationName[3].equals("Digest") && applicationName[4].equals("SHA-0")) {
                                String hashC = hashController.hashSHA0(applicationName[5]);
                                Core.messageTo(packet, "Execute|The hash code in SHA-0 is: " + hashC + ".", ida);
                            } else if (applicationName[3].equals("Digest") && applicationName[4].equals("SHA-256")) {
                                String hashC = hashController.hashSHA256(applicationName[5]);
                                Core.messageTo(packet, "Execute|The hash code in SHA-256 is: " + hashC + ".", ida);
                            }
                        } else if (applicationName.length == 4) {
                            if (applicationName[3].equals("List")) {
                                List<String> ids = hashController.hashList();
                                Core.messageTo(packet, "Execute|The identifiers available are:", ida);
                                for (String s : ids) {
                                    Core.messageTo(packet, "Execute|" + s, ida);
                                }
                            } else if (applicationName[3].equals("help")) {
                                Core.messageTo(packet, "Execute|\nFollowing codes are available: ", ida);
                                Core.messageTo(packet, "Execute|- Digest(identifier,text)", ida);
                                Core.messageTo(packet, "Execute|- List", ida);
                                Core.messageTo(packet, "Execute|- List", ida);
                                Core.messageTo(packet, "Execute|Insert your code with the correct format: (0 or Exit to End Application)", ida);
                            }
                        } else {
                            Core.messageTo(packet, "Execute|Invalid input. Please try again.", ida);
                        }
                    }
                }
            } else {
                Core.message(packet, "Execute|The required application is not open!");
            }
        }
    }
}
