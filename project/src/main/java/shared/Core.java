package shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;

public class Core {

    /**
     * Variable that controls globally if the app is supposed to keep running
     */
    static boolean continueApp = true;
    /**
     * List of IP addresses
     */
    private static HashSet<InetAddress> peersList = new HashSet<>();

    /**
     * Method that sends a specific message to all IP's
     *
     * @param udpPacket udpPacket object
     * @param frase     String with information
     * @throws Exception
     */
    public static void message(DatagramPacket udpPacket, String frase) throws Exception {
        byte[] fraseData = frase.getBytes();
        udpPacket.setData(fraseData);
        udpPacket.setLength(frase.length());
        sendToAll(sock, udpPacket);
    }

    /**
     * Method that sends a specific message to a specific IP
     *
     * @param udpPacket udpPacket object
     * @param frase     String with information
     * @param inA       InetAddress object that contains a specific IP
     * @throws Exception
     */
    public static void messageTo(DatagramPacket udpPacket, String frase, InetAddress inA) throws Exception {
        byte[] fraseData = frase.getBytes();
        udpPacket.setData(fraseData);
        udpPacket.setLength(frase.length());
        sendTo(sock, udpPacket, inA);
    }

    /**
     * IP address used in the broadcast system by default
     */
    private static final String BCAST_ADDR = "255.255.255.255";
    /**
     * Port defined for the group
     */
    private static final int SERVICE_PORT = 30101;

    /**
     * Main class of the program.
     * Runs the message system to be able to send messages to applications and runs the main user interface class,
     * for the user to choose which applications to run.
     *
     * @param args arguments of the application
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        mainUI mi = new mainUI();

        byte[] data = new byte[300];
        DatagramPacket udpPacket;

        //creates network socket as an internal endpoint for sending or receiving a message between applications
        try {
            sock = new DatagramSocket(SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        //Gets the InetAddress Object related to the IP address in the variable BCAST_ADDR
        bcastAddress = InetAddress.getByName(BCAST_ADDR);
        sock.setBroadcast(true);
        data[0] = 1;
        //Creates a udpPacket DatagramPacket object which will contain the message's data to send or recieve
        udpPacket = new DatagramPacket(data, 1, bcastAddress, SERVICE_PORT);
        sock.send(udpPacket);
        Thread udpReceiver = new Thread(new UdpChatReceive(sock, udpPacket, args));
        //Starts the udpReceiver thread to open the messaging system between the applications
        udpReceiver.start();
        mi.mainUiPresentation(udpPacket, args);
        data[0] = 0; // announce its leaving
        udpPacket.setData(data);
        udpPacket.setLength(1);
        sendToAll(sock, udpPacket);
        sock.close();
        udpReceiver.join(); // wait for the thread to end

    }

    /**
     * Method that adds an IP address to the list
     *
     * @param ip
     */
    public static synchronized void addIP(InetAddress ip) {
        peersList.add(ip);
    }

    /**
     * Returns the list of IP addresses
     *
     * @return
     */
    public static synchronized HashSet<InetAddress> getIpList() {
        return peersList;
    }

    /**
     * Removes an IP address from the list
     *
     * @param ip InetAddtess object
     */
    public static synchronized void remIP(InetAddress ip) {
        peersList.remove(ip);
    }

    /**
     * Prints the list of IP addresses in the list
     */
    public static synchronized void printIPs() {
        for (InetAddress ip : peersList) {
            System.out.print(" " + ip.getHostAddress());
        }
    }

    /**
     * Sends to all the IP addresses in the list a message saved in the DatagramPacket object
     *
     * @param s Network socket
     * @param p DatagramPacket containing message
     * @throws Exception
     */
    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception {
        for (InetAddress ip : peersList) {
            p.setAddress(ip);
            s.send(p);
        }
    }

    /**
     * Sends to a specific IP address a message saved in the DatagramPacket object
     *
     * @param s   Network socket
     * @param p   DatagramPacket containing message
     * @param inA InetAddress with the specific IP address
     * @throws Exception
     */
    public static synchronized void sendTo(DatagramSocket s, DatagramPacket p, InetAddress inA) throws Exception {
        p.setAddress(inA);
        s.send(p);
    }

    /**
     * InetAddress object to open the communication messaging system
     */
    static InetAddress bcastAddress;

    /**
     * Network socket to open the communication messaging system
     */
    static DatagramSocket sock;

}
