package shared;

import java.net.DatagramPacket;
import java.util.concurrent.TimeUnit;

/**
 * Method that handles repeated calls from another methods. Supports multithreading
 */
public class RepeatedCalls extends Thread {

    /**
     * Message to repeat
     */
    private String call;

    /**
     * DatagramPacket
     */
    private DatagramPacket udpPacket;

    /**
     * Thread control
     */
    private boolean canGo;

    /**
     * Public constructor
     *
     * @param call      message to repeat
     * @param udpPacket DatagreamPacket
     */
    public RepeatedCalls(String call, DatagramPacket udpPacket) {
        this.call = call;
        this.udpPacket = udpPacket;
    }

    /**
     * Change the thread status to canGo
     *
     * @param canGo multithread continues or not
     */
    public void canGo(boolean canGo) {
        this.canGo = canGo;
    }

    /**
     * Method that is always sending to each application the status of this application
     */
    public void run() {
        while (canGo) {
            try {
                Core.message(udpPacket, call);
                TimeUnit.SECONDS.sleep(5);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
