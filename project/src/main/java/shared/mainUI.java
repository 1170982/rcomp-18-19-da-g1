package shared;

import TCPmonitor.TCPmonitorUI;
import convert.ConvertUI;
import hash.HashUI;
import queue.QueueUI;
import stack.StackUI;
import var.VarUI;

import java.net.DatagramPacket;
import java.util.InputMismatchException;
import java.util.Scanner;

public class mainUI {

    private static Scanner in = new Scanner(System.in);

    public void mainUiPresentation(DatagramPacket udpPacket, String[] args) throws Exception {
        System.out.println(" -----##### RCOMP - SPRINT 5 PROJECT #####-----\n\n");
        int option = 8;
        while (option != 0 && Core.continueApp) {
            boolean number = false;
            while (!number && Core.continueApp) {
                System.out.println("\nApplications: \n");
                System.out.println("1 - Var");
                System.out.println("3 - Hash");
                System.out.println("4 - NumConvert");
                System.out.println("5 - Stack");
                System.out.println("6 - TCPMonitor");
                System.out.println("7 - Queue");
                System.out.println("Choose a number: (0 to End Project)");
                try {
                    option = in.nextInt();
                    number = true;
                } catch (InputMismatchException e) {
                    number = false;
                    System.out.println("Choose a correct number !");
                    in.nextLine();
                }
            }
            switch (option) {
                case 0:
                    Core.continueApp = false;
                    System.out.println("Application terminated !");
                    break;
                case 1:
                    //Var
                    VarUI vUI = new VarUI();
                    Core.message(udpPacket, "Var|Start");
                    vUI.presentation(udpPacket);
                    break;
                case 3:
                    HashUI hUI = new HashUI();
                    Core.message(udpPacket, "Hash|Start");
                    hUI.presentation(udpPacket);
                    break;
                case 4:
                    ConvertUI cUI = new ConvertUI();
                    Core.message(udpPacket, "NumConvert|Start");
                    cUI.presentation(udpPacket);
                    break;
                case 5:
                    //Stack
                    StackUI sui = new StackUI();
                    Core.message(udpPacket, "Stack|Start");
                    sui.presentation(udpPacket);
                    break;
                case 6:
                    TCPmonitorUI tcpui = new TCPmonitorUI();
                    Core.message(udpPacket, "TCP|Start");
                    tcpui.presentation(udpPacket);
                    break;
                case 7:
                    QueueUI qUI = new QueueUI();
                    //udpReceiver.notify();
                    Core.message(udpPacket, "Queue|Start");
                    qUI.presentation(udpPacket);
                    break;
                default:
                    System.out.println("\nOption does not exit!\n");
                    break;
            }
        }
    }
}
