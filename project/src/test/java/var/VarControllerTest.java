package var;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class VarControllerTest {

    @Test
    public void storeTest() {
        VarController vc = new VarController();
        String information = vc.store("iD", "text message for the var");
        assertEquals(information, "text message for the var");
        information = vc.store("iD", "another text message for the var");
        assertEquals(information, "another text message for the var");
        vc.store("iD", "");
        List<String> response = vc.list();
        assertEquals(response.size(), 0);
    }

    @Test
    public void fetchTest() {
        VarController vc = new VarController();
        vc.store("iD", "text message for the var");
        String fetchInformation = vc.fetch("iD");
        assertEquals("text message for the var", fetchInformation);
        fetchInformation = vc.fetch("iD2");
        assertEquals("\nVar does not exist!", fetchInformation);
    }

    @Test
    public void listTest() {
        VarController vc = new VarController();
        vc.store("iD", "text message for the var");
        vc.store("iD2", "another text message for the var");
        List<String> response = vc.list();
        assertEquals(response.get(0), "iD");
        assertEquals(response.get(1), "iD2");
    }

    @Test
    public void eraseTest() {
        VarController vc = new VarController();
        vc.store("iD", "text message for the var");
        String informationE = vc.erase("iD");
        assertEquals("text message for the var", informationE);
        List<String> response = vc.list();
        assertEquals(response.size(), 0);
        informationE = vc.erase("iD2");
        assertEquals("\nVar with identifier does not exist!", informationE);

    }
}