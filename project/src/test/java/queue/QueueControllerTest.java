package queue;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class QueueControllerTest {

    @Test
    public void enqueueTest(){
        QueueController qc = new QueueController();
        qc.enqueue("iD", "text message for the queue");
        int size = qc.enqueue("iD", "another text message for the queue");
        assertEquals(size,2);
        Map<String,Integer> informationMap = qc.list();
        assertEquals(2,(int)informationMap.get("iD"));
    }
    @Test
    public void enqueueNoqueueCreatedBeforeTest(){
        QueueController qc = new QueueController();
        int size = qc.enqueue("iD", "text message for the queue");
        assertEquals(1, size);
        Map<String,Integer> informationMap = qc.list();
        assertEquals(1,(int)informationMap.get("iD"));
    }

    @Test
    public void dequeueTest(){
        QueueController qc = new QueueController();
        qc.enqueue("iD", "text message for the queue");
        String text = qc.dequeue("iD");
        assertEquals("text message for the queue", text);

        qc.enqueue("iD", "text message for the queue");
        qc.enqueue("iD2", "text message for the queue");

        text = qc.dequeue("iD");
        assertEquals("text message for the queue", text);
        text = qc.dequeue("iD2");
        assertEquals("text message for the queue", text);

        text = qc.dequeue("iD");
        assertEquals("There were no elements in the queue!", text);

        text = qc.dequeue("doesnt exist");
        assertEquals("Queue with identifier does not exist !", text);
    }

    @Test
    public void removeTest(){
        QueueController qc = new QueueController();
        qc.enqueue("iD", "text message for the queue");
        String text = qc.remove("iD");
        assertEquals("Removed with success !", text);

        text = qc.remove("doesnt exist");
        assertEquals("\nQueue with identifier does not exist !", text);

        text = qc.dequeue("iD");
        assertEquals("Queue with identifier does not exist !",text);
    }

    @Test
    public void listTest(){
        QueueController qc = new QueueController();
        qc.enqueue("iD", "something");
        qc.enqueue("iD2", "something2");
        qc.enqueue("iD2", "something3");
        Map<String,Integer> informationMap = qc.list();
        assertEquals(1,(int)informationMap.get("iD"));
        assertEquals(2,(int)informationMap.get("iD2"));
    }

}