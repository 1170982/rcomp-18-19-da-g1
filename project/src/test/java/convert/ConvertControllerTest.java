/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert;


import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author catar
 */
public class ConvertControllerTest {
    
    @Test
    public void testConvertBO() {
        System.out.println("convertBIN_OCT");
        String ir = "BIN";
        String or = "OCT";
        String number = "1110";
        ConvertController instance = new ConvertController();
        String expResult = "16";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertBOE() {
        System.out.println("convertBIN_OCT_ERR");
        String ir = "BIN";
        String or = "OCT";
        String number = "99";
        ConvertController instance = new ConvertController();
        String expResult = "Error on the number inserted, please insert the number in the right format";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertBH() {
        System.out.println("convertBIN_HEX");
        String ir = "BIN";
        String or = "HEX";
        String number = "1010";
        ConvertController instance = new ConvertController();
        String expResult = "a";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertBD() {
        System.out.println("convertBIN_DEC");
        String ir = "BIN";
        String or = "DEC";
        String number = "10";
        ConvertController instance = new ConvertController();
        String expResult = "2";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }

    @Test
    public void testConvertOB() {
        System.out.println("convertOCT_BIN");
        String ir = "OCT";
        String or = "BIN";
        String number = "13";
        ConvertController instance = new ConvertController();
        String expResult = "1011";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertOBE() {
        System.out.println("convertOCT_BIN_ERR");
        String ir = "OCT";
        String or = "BIN";
        String number = "999";
        ConvertController instance = new ConvertController();
        String expResult = "Error on the number inserted, please insert the number in the right format";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertOH() {
        System.out.println("convertOCT_HEX");
        String ir = "OCT";
        String or = "HEX";
        String number = "13";
        ConvertController instance = new ConvertController();
        String expResult = "b";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertOD() {
        System.out.println("convertOCT_DEC");
        String ir = "OCT";
        String or = "DEC";
        String number = "13";
        ConvertController instance = new ConvertController();
        String expResult = "11";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertHB() {
        System.out.println("convertHEX_BIN");
        String ir = "HEX";
        String or = "BIN";
        String number = "f";
        ConvertController instance = new ConvertController();
        String expResult = "1111";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertHBE() {
        System.out.println("convertHEX_BIN_ERR");
        String ir = "HEX";
        String or = "BIN";
        String number = "4G";
        ConvertController instance = new ConvertController();
        String expResult = "Error on the number inserted, please insert the number in the right format";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testConvertHO() {
        System.out.println("convertHEX_OCT");
        String ir = "HEX";
        String or = "OCT";
        String number = "f";
        ConvertController instance = new ConvertController();
        String expResult = "17";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertHOE() {
        System.out.println("convertHEX_OCT_ERR");
        String ir = "HEX";
        String or = "OCT";
        String number = "HOHHOHHOH";
        ConvertController instance = new ConvertController();
        String expResult = "Error on the number inserted, please insert the number in the right format";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testConvertHD() {
        System.out.println("convertHEX_DEC");
        String ir = "HEX";
        String or = "DEC";
        String number = "f";
        ConvertController instance = new ConvertController();
        String expResult = "15";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testConvertHDN() {
        System.out.println("convertHEX_DEC_NEG");
        String ir = "HEX";
        String or = "DEC";
        String number = "-f";
        ConvertController instance = new ConvertController();
        String expResult = "-15";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    
     @Test
    public void testConvertDB() {
        System.out.println("convertDEC_BIN");
        String ir = "DEC";
        String or = "BIN";
        String number = "200";
        ConvertController instance = new ConvertController();
        String expResult = "11001000";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertDBE() {
        System.out.println("convertDEC_BIN_ERR");
        String ir = "DEC";
        String or = "BIN";
        String number = "aaaa";
        ConvertController instance = new ConvertController();
        String expResult = "Error on the number inserted, please insert the number in the right format";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testConvertDO() {
        System.out.println("convertDEC_OCT");
        String ir = "DEC";
        String or = "OCT";
        String number = "200";
        ConvertController instance = new ConvertController();
        String expResult = "310";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testConvertDH() {
        System.out.println("convertDEC_HEX");
        String ir = "DEC";
        String or = "HEX";
        String number = "200";
        ConvertController instance = new ConvertController();
        String expResult = "c8";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testConvertDHN() {
        System.out.println("convertDEC_HEX_NEG");
        String ir = "DEC";
        String or = "HEX";
        String number = "-1";
        ConvertController instance = new ConvertController();
        String expResult = "ffffffff";
        String result = instance.convert(ir, or, number);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of list method, of class ConvertController.
     */
    @Test
    public void testList() {
        System.out.println("list");
        ConvertController cc = new ConvertController();
        List<String> expResult = new ArrayList<String>();
        expResult.add("BIN");
        expResult.add("OCT");
        expResult.add("HEX");
        expResult.add("DEC");
        List<String> result = cc.list();
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
        assertEquals(expResult.get(2), result.get(2));
        assertEquals(expResult.get(3), result.get(3));


        



    }
    
}
