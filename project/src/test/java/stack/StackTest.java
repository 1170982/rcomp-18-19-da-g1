package stack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StackTest {

    Stack stack;

    @Before
    public void setUp() throws Exception {
        stack = new Stack("test");
        stack.pushSentence("first");
    }

    @Test
    public void getIdentifier() {
        assertEquals("test", stack.getIdentifier());
    }

    @Test
    public void pushAndPop() {
        assertEquals(2, stack.pushSentence("second"));
        assertEquals("second", stack.popSentence());
    }

}