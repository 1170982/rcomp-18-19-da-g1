package stack;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackControllerTest {

    StackController sc;

    public StackControllerTest(){
        sc = new StackController();
        sc.push("test1", "first");
    }

    @Test
    public void pushAndPop() {
        assertEquals(1, sc.push("test2", "first"));
        assertEquals(2, sc.push("test1", "second"));
        assertEquals("second", sc.pop("test1"));
        assertEquals("first", sc.pop("test2"));
    }

    @Test
    public void list() {
        //assertEquals(1, sc.list().size());
        for (Stack s: sc.list()){
            assertEquals("test1", s.getIdentifier());
        }
    }
}