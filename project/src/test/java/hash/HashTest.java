package hash;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;


public class HashTest {

    @Test
    public void hashMD5() throws NoSuchAlgorithmException {
        Hash h = new Hash();
        String value = h.hash("MD5","olá");
        assertEquals("AB11FA17FF3337A200CB2714DBC2318C",value);
    }

    @Test
    public void hashSHA0() throws NoSuchAlgorithmException {
        Hash h = new Hash();
        String value = h.hash("SHA-1","olá");
        assertEquals("3E16889D494632F3C528FB3A3756435F8AA88788",value);
    }

    @Test
    public void hashSHA256() throws NoSuchAlgorithmException {
        Hash h = new Hash();
        String value = h.hash("SHA-256","olá");
        assertEquals("9B186E077C7C6D044F5789D76E6D8070A5B0AAA902EBC608BC34170722DBA903",value);
    }

    @Test
    public void hashError() throws NoSuchAlgorithmException {
        Hash h = new Hash();
        String value = h.hash("aaa","olá");
        assertEquals("",value);
    }
}