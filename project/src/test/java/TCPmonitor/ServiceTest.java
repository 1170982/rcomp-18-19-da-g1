package TCPmonitor;

import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceTest {

    @Test
    public void setStatusTest() {
        Service s1 = new Service("service1", "255.255.255.255", 9999);
        assertEquals(s1.getStatus(), "UNCHECKED");
        s1.setStatus("available");
        assertEquals(s1.getStatus(), "AVAILABLE");
    }

    @Test
    public void equalsTest() {
        Service s1 = new Service("service1", "255.255.255.255", 9999);
        assertFalse(s1.equals(null));
        assertFalse(s1.equals(new String()));
        assertTrue(s1.equals(new Service("service1", "255.255.255.255", 9999)));
        assertFalse(s1.equals(new Service("service2", "255.255.255.254", 9999)));
    }
}