package TCPmonitor;

import org.junit.Test;

import static org.junit.Assert.*;

public class TCPmonitorControllerTest {

    private TCPmonitorController tcpmc;

    public TCPmonitorControllerTest(){
        tcpmc = new TCPmonitorController();
    }

    @Test
    public void addService() {
        assertTrue(tcpmc.getServices().isEmpty());
        tcpmc.addService("service1", "255.255.255.255", 9999);
        Service s1 = new Service("service1", "255.255.255.255", 9999);
        assertEquals(s1, tcpmc.getServices().get(0));
        tcpmc.removeService("service1");
        assertTrue(tcpmc.getServices().isEmpty());
    }
}