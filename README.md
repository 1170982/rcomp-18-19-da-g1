RCOMP 2018-2019 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1170561 - Ana Rita Fernandes
  * 1170694 - João Tomás Rodrigues
  * 1170723 - Ricardo Matos
  * 1170982 - Henrique Ribeiro
  * 1170997 - Rui Pedro Machado
  * 1171138 - Catarina Fernandes

Any team membership changes should be reported here, examples:

Member 8888888 ({First and last name}) has left the team on 2019-03-20

Member 7777777 ({First and last name}) has entered the team on 2019-04-5
# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)
  * [Sprint 4](doc/sprint4/)
  * [Sprint 5](doc/sprint5/)
  * [Sprint 6](doc/sprint6/)
